package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezspec.visitor.PlainTextReport;

import java.io.File;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

@EzFeature
@EzFeatureReport(language="zh-TW")
//@EzFeatureReport
public class PlainTextReportWithDefaultStorySpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("PlainTextReport");
    }

    @Test
    public void write_to_file() {
        feature.newDefaultStory()
                .newScenario("scenario name")
                .Given("a given", env -> {
                })
                .And("that the lead waits in the queue to the tenants pipeline", env -> {
                })
                .When("the tenant lead has a score", env -> {
                })
                .Then("we should result the lead", env -> {
                }).Execute();

        PlainTextReport visitor = new PlainTextReport();

        feature.accept(visitor);

        visitor.writeToFile("./target/write_to_file_default.txt");

        StringBuilder sb = new StringBuilder();

        try {
            File file = new File("./target/write_to_file_default.txt");
            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                sb.append(sc.nextLine());
                sb.append("\n");
            }
        } catch (Exception e) {
            fail(e);
        }

        var expected = """
                Feature: PlainTextReport
                                
                Scenario: scenario name
                [Success] Given a given
                [Success] And that the lead waits in the queue to the tenants pipeline
                [Success] When the tenant lead has a score
                [Success] Then we should result the lead
                """;
        assertEquals(expected, sb.toString());
    }
}
