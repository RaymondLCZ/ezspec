package tw.teddysoft.ezspec.i18n;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

//{"and":["* ","而且","並且","同時"],"background":["背景"],"but":["* ","但是"],"examples":["例子"],"feature":["功能"],"given":["* ","假如","假設","假定"],"name":"Chinese traditional","native":"繁體中文","rule":["Rule"],"scenario":["場景","劇本"],"scenarioOutline":["場景大綱","劇本大綱"],"then":["* ","那麼"],"when":["* ","當"]}

public class GherkinKeywords {

    private int index = 0;

    public ArrayList<String> and;
    public ArrayList<String> background;
    public ArrayList<String> but;
    public ArrayList<String> examples;
    public ArrayList<String> feature;
    public ArrayList<String> given;
    public String name;
    @JsonProperty("native")
    public String mynative;
    public ArrayList<String> rule;
    public ArrayList<String> scenario;
    public ArrayList<String> scenarioOutline;
    public ArrayList<String> then;
    public ArrayList<String> when;

    public String getI18nKeyword(String keyword){
        return switch (keyword.toUpperCase()){
            case "AND" -> and.get(index);
            case "BACKGROUND" -> background.get(index);
            case "BUT" -> but.get(index);
            case "EXAMPLES" -> examples.get(index);
            case "FEATURE" -> feature.get(index);
            case "GIVEN" -> given.get(index);
            case "RULE" -> rule.get(index);
            case "SCENARIO" -> scenario.get(index);
            case "SCENARIO OUTLINE" -> scenarioOutline.get(index);
            case "THEN" -> then.get(index);
            case "WHEN" -> when.get(index);
            default -> throw new RuntimeException("Unsupported keyword: " + keyword);
        };

    }

}

