package tw.teddysoft.ezspec.visitor;


import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.Story;
import tw.teddysoft.ezspec.example.Example;
import tw.teddysoft.ezspec.i18n.GherkinKeywords;
import tw.teddysoft.ezspec.scenario.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.String.format;

/**
 * {@code PlainTextReport} is a class for generating reports.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class PlainTextReport implements SpecificationElementVisitor {
    private final StringBuilder output = new StringBuilder();

    private GherkinKeywords gherkinKeywords;
    public PlainTextReport(){
        gherkinKeywords = null;
    }

    public PlainTextReport(GherkinKeywords gherkinKeywords){
        this.gherkinKeywords = gherkinKeywords;
    }

    @Override
    public void visit(SpecificationElement element) {
        switch (element){
            case Feature feature -> {
                output.append(format("%s: %s", getI18NKeyword(Feature.KEYWORD), feature.getName()));
                if (!feature.getDescription().isEmpty())
                    output.append(format("\n%s", feature.getDescription()));
            }
            case Story story ->  {
                output.append(format("\n\nAbility: %s\n\n%s", story.getName(), story.description()));
            }
            case Background background ->  {
//                output.append(background);
                if (background.toString().isEmpty()) return;
                output.append(format("\n%s: %s", getI18NKeyword(Background.KEYWORD), background.getName()));
                for(var each : background.steps()){
                    output.append(format("\n%s %s", getI18NKeyword(each.getName()), each.description()));
                }
            }
            case ScenarioOutline scenarioOutline ->  {

                var x = getI18NKeyword(ScenarioOutline.KEYWORD);
                output.append(format("%s: %s\n\n", getI18NKeyword(ScenarioOutline.KEYWORD), scenarioOutline.getDisplayName()));
                if (!scenarioOutline.getDescription().isEmpty())
                    output.append(format("%s\n", scenarioOutline.getDescription()));

                for (var each : scenarioOutline.getSteps()) {
                    output.append(each.getName());
                    if (!each.description().isEmpty()) {
                        output.append(" ").append(each.description());
                    }
                    output.append("\n");
                }

                scenarioOutline.getAllExamples().forEach(example -> {
                    output.append(format("\n%s: %s\n", getI18NKeyword(Example.KEYWORD), example.getName()));
                    if (!example.getDescription().isEmpty())
                        output.append(format("%s\n", example.getDescription()));

                    output.append(example.getTable().toString());
                });

//                output.append(format("\n\n%s", scenarioOutline));
            }
            case RuntimeScenario runtimeScenario -> {
                if (runtimeScenario.isFromScenarioOutline()){
                    String exampleName;
                    exampleName = runtimeScenario.activeTable().get("example_code");
                    output.append(format("\n[%d] %s", Integer.valueOf(runtimeScenario.getIndex() + 1), exampleName));
                } else {
                    output.append(format("\n\n%s: %s", getI18NKeyword(Scenario.KEYWORD), runtimeScenario.getReplacedUnderscoresName()));
                }

            }
            case Step step ->  {
                output.append(format("\n[%s] %s %s", step.getResult(), getI18NKeyword(step.getName()), step.description()));
                switch (step.getResult().getExecutionOutcome()){
                    case Failure -> {
                        output.append(format("\n\t\t\tat %s", step.getResult().getFailureMessage()));
                    }
                    case Pending -> {
                        if (!step.getResult().getFailureMessage().isEmpty())
                            output.append(format("\n\t\t\tcaused by %s", step.getResult().getFailureMessage()));
                    }
                    default -> {}
                }
            }
            default -> {}
        }
    }

    public String getI18NKeyword(String keyword){
        if (null != gherkinKeywords){
            return gherkinKeywords.getI18nKeyword(keyword);
        }
        return keyword;
    }

    public String getOutput() {
        return output.toString();
    }

    public void writeToFile(String fileName){
        try (FileWriter fileWriter = new FileWriter(fileName);
             PrintWriter printWriter = new PrintWriter(fileWriter)) {
            printWriter.print(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
