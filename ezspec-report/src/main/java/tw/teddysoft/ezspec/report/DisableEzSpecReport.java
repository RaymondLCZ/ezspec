package tw.teddysoft.ezspec.report;

import org.apiguardian.api.API;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@API(
        status = API.Status.EXPERIMENTAL,
        since = "1.0"
)
public @interface DisableEzSpecReport {
}

