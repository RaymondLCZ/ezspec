package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.table.Header;

import java.util.List;

public record HeaderDto(List<String> header) {
    public static HeaderDto of(Header aHeader){
        return new HeaderDto(aHeader.header().stream().toList());
    }
}
