package tw.teddysoft.ezspec.report;

import tw.teddysoft.ezspec.scenario.Scenario;

import java.util.List;

public record ScenarioDto(String keyword, String name, List<StepDto> stepDtos) implements SpecificationElementDto {
    public static ScenarioDto of(Scenario scenario) {
        return new ScenarioDto(Scenario.KEYWORD, scenario.getName(), StepDto.of(scenario.steps()));
    }

    public static List<ScenarioDto> of(List<Scenario> scenarios) {
        return scenarios.stream().map(ScenarioDto::of).toList();
    }
}
