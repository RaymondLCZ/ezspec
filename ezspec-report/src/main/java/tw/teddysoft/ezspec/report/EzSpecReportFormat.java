package tw.teddysoft.ezspec.report;

import org.apiguardian.api.API;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@API(
        status = API.Status.EXPERIMENTAL,
        since = "1.0"
)
@Documented
public @interface EzSpecReportFormat {
    Format [] value();

    enum Format {
        json,
        txt,
        html
    }
}

