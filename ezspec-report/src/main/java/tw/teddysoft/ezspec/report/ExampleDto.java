package tw.teddysoft.ezspec.report;

import tw.teddysoft.ezspec.example.Example;

import java.util.List;

public record ExampleDto(String name, String description, TableDto tableDto) {
    public static ExampleDto of(Example example) {
        return new ExampleDto(example.getName(), example.getDescription(), TableDto.of(example.getTable()));
    }

    public static List<ExampleDto> of(List<Example> examples) {
        return examples.stream().map(ExampleDto::of).toList();
    }
}
