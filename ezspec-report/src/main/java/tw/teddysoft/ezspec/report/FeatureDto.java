package tw.teddysoft.ezspec.report;

import tw.teddysoft.ezspec.Feature;

import java.util.Arrays;
import java.util.List;

public record FeatureDto(String keyword, String name, String description, List<StoryDto> storyDtos) {
    public static FeatureDto of(Feature feature) {
        if (null == feature.getDefaultStory()) {
            return new FeatureDto(Feature.KEYWORD, feature.getName(), feature.getDescription(), StoryDto.of(feature.getStories()));
        }
        return new FeatureDto(Feature.KEYWORD, feature.getName(), feature.getDescription(), StoryDto.of(Arrays.asList(feature.getDefaultStory())));
    }
}
