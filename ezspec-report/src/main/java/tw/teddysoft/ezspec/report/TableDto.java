package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.table.Table;

import java.util.List;

public record TableDto(HeaderDto headerDto, List<RowDto> rows, String RawData) {

    public static TableDto of(Table table) {
        return new TableDto(HeaderDto.of(table.header()), RowDto.of(table.rows()), table.getRawData());
    }
}
