package tw.teddysoft.ezspec.report;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "@class")
@JsonSubTypes({@JsonSubTypes.Type(value = ScenarioOutlineDto.class), @JsonSubTypes.Type(value = ScenarioDto.class)})
public interface SpecificationElementDto extends Serializable {
}
