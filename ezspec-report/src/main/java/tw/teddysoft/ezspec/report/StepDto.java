package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.scenario.Step;

import java.util.List;

public class StepDto {
    public String keyword;
    public String description;
    public String stepExecutionOutcome;
    public String errorMessage;

    public String exception;
    public String stackTrace;
    public boolean continuousAfterFailure;

    public StepDto(){
        exception = "";
        stackTrace = "";
    }

    public static StepDto of(Step step) {
        StepDto stepDto = new StepDto();
        stepDto.keyword = step.getName();
        stepDto.description = step.description();
        stepDto.continuousAfterFailure = step.isContinuousAfterFailure();
        stepDto.stepExecutionOutcome = step.getResult().getExecutionOutcome().name();
        stepDto.errorMessage = step.getResult().getFailureMessage();

        if (null != step.getResult().getException()){
            stepDto.exception = step.getResult().getException().toString();
            stepDto.stackTrace = step.getResult().getStackTrace();
        }

        return stepDto;
    }

    public static List<StepDto> of(List<Step> steps) {
        return steps.stream().map(StepDto::of).toList();
    }
}
