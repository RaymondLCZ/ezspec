package tw.teddysoft.ezspec.report;


import tw.teddysoft.ezspec.Story;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;

import java.util.ArrayList;
import java.util.List;

public record StoryDto(String keyword, String name, String description, int order, BackgroundDto backgroundDto, List<SpecificationElementDto> specificationElementDtos) {

    public static StoryDto of(Story story){
        List<SpecificationElementDto> specificationElementDtos = new ArrayList<>();
        story.getScenarios().forEach(scenario -> {
            switch (scenario){
                case ScenarioOutline scenarioOutline -> specificationElementDtos.add(ScenarioOutlineDto.of(scenarioOutline));
                default -> specificationElementDtos.add(ScenarioDto.of(scenario));
            }
        });

        return new StoryDto(Story.KEYWORD, story.getName(), story.description(), story.order(), BackgroundDto.of(story.getBackground()), specificationElementDtos);
    }

    public static List<StoryDto> of(List<Story> stories){
        return stories.stream().map(StoryDto::of).toList();
    }
}
