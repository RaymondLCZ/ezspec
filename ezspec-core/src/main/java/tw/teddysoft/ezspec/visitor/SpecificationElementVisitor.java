package tw.teddysoft.ezspec.visitor;

/**
 * {@code SpecificationElementVisitor} is the interface for visiting all elements.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public interface SpecificationElementVisitor {

    void visit (SpecificationElement element);
}
