package tw.teddysoft.ezspec.scenario;


import tw.teddysoft.ezspec.Story;

import java.util.Objects;

import static java.lang.String.format;

/**
 * {@code Background} is a class for representing Gherkin background.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Background extends RuntimeScenario {

    public static final String KEYWORD = "Background";

    public static final Background DEFAULT = new Background("");

    /**
     * A static factory to create a background.
     *
     * @param story the story of this background
     * @return a new background instance
     */
    public static Background New(Story story){
        Objects.requireNonNull(story, "story");

        return story.newBackground(getEnclosingMethodName());
    }

    /**
     * A static factory to create a background.
     *
     * @param name the name of this background
     * @param story the story of this background
     * @return a new background instance
     */
    public static Background New(String name, Story story){
        Objects.requireNonNull(name, "name");

        return story.newBackground(name);
    }

    /**
     * Instantiates a new Background.
     *
     * @param name the name of this background
     */
    public Background(String name){
        super(name);
    }

    /**
     * Background text string including background name, step name
     * and step description.
     *
     * @return the string
     */
    @Override
    public String toString() {

        if (steps().isEmpty()) return "";

        StringBuilder sb = new StringBuilder();
        sb.append(format("Background: %s", getReplacedUnderscoresName()));

        for(var each : steps()){
            sb.append(format("\n%s %s", each.getName(), each.description()));
        }
        return sb.toString();
    }

}
