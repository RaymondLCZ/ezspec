package tw.teddysoft.ezspec.scenario;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@code Argument} is a class for defining variables in step description.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Argument {
    private  String key;
    private  String value;

    /**
     * Static factory for creating an Argument.
     *
     * @param expression the expression
     * @return an argument instance
     */
    public static Argument create(String expression){
        return new Argument(expression);
    }

    /**
     * Static factory for creating an Argument with only key.
     *
     * @param key the key
     * @return an argument instance
     */
    public static Argument fromKey(String key){
        var arg = new Argument();
        arg.key(key);
        return arg;
    }

    private Argument(){};

    private boolean isKeyValue(String expression){
        return expression.trim().startsWith("${") && expression.trim().endsWith("}");
    }

    /**
     * Instantiates a new Argument.
     *
     * @param expression the expression
     */
    public Argument(String expression) {

        if (isKeyValue(expression)){
            String regx = "^([^=:]+)[=:](.+)$";
            Pattern pattern = Pattern.compile(regx);
            Matcher matcher = pattern.matcher(expression);
            if (matcher.find()) {
                    this.key = matcher.group(1).trim().substring(2);
                    this.value = matcher.group(2).trim().substring(0, matcher.group(2).trim().length()-1);
            }
        }
        else{
            String regx = "\\$(\\S)+";
            Pattern pattern = Pattern.compile(regx);
            Matcher matcher = pattern.matcher(expression);
            if (matcher.find()) {
                this.key = "";
                this.value =  matcher.group(0).trim().substring(1);
            }
        }
    }

    public String key() {
        return key;
    }

    public String value() {
        return value;
    }

    public void key(String key) {
        this.key = key;
    }

    public void value(String value) {
        this.value = value;
    }

}
