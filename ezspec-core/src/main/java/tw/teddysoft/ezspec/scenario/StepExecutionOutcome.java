package tw.teddysoft.ezspec.scenario;

/**
 * {@code StepExecutionOutcome} is the enum for Step execution outcome.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public enum StepExecutionOutcome {
    /**
     * Default step status or this step has not been implemented.
     */
    Pending,
    /**
     * Success step execution outcome.
     */
    Success,
    /**
     * Failure step execution outcome.
     */
    Failure,
    /**
     * The past step failed and this step was not executed.
     */
    Skipped;
}
