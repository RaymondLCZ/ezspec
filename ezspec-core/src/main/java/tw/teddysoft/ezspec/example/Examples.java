package tw.teddysoft.ezspec.example;


import tw.teddysoft.ezspec.table.Table;

import java.util.Objects;

import static java.lang.String.format;

/**
 * {@code Examples} is an interface for representing Gherkin examples.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public interface Examples {

    static Example New(String tableContent) {
        Objects.requireNonNull(tableContent, "table content");

        return new Example(tableContent);
    }

    String getName();

    String getDescription();

    Table getTable();

    default Example getExample(){
        return new Example(getName(), getDescription(), getTable());
    }
}
