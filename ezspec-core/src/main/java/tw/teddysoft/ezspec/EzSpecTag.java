package tw.teddysoft.ezspec;

public interface EzSpecTag {
    interface Env {
        String Dev = "Env.Development";
        String Testing = "Env.Testing";
        String Staging = "Env.Staging";
        String Production = "Env.Production";
    }

    interface TestType{
        String Unit = "TestType.Unit";
        String Integration = "TestType.Integration";
        String UseCase = "TestType.UseCase";
        String EndToEnd = "TestType.EndToEnd";
        String Sanity = "TestType.Sanity";
        String AssertionFree = "TestType.AssertionFree";
        String Trivial = "TestType.Trivial";
        String Report = "TestType.Report";
    }

    interface CQRS {
        String Command = "CQRS.Command";
        String Query = "CQRS.Query";
    }
    interface DI {
        String RAM = "DI.RAM";
        String SpringBoot = "DI.SpringBoot";
    }
    interface Other {
    }

    interface Speed {
        String Fast = "Speed.Fast";
        String Slow = "Speed.Slow";
    }

    interface LivingDoc{
        String EzSpec = "LivingDoc.EzSpec";
    }

}
