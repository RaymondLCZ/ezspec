package tw.teddysoft.ezspec.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.String.format;

/**
 * {@code Header} is a class for representing header in {@code Table}.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Header {
    private final List<String> header;

    public static Header valueOf(List<String> data){
        return new Header(data);
    }

    public static Header create(){
        return new Header();
    }

    public Header(Header that){
        this();
        this.header.addAll(that.header);
    }
    private Header(List<String> data){
        this();
        this.header.addAll(data);
    }

    public void reset(List<String> newHeader){
        header.clear();
        header.addAll(newHeader);
    }
    private Header(){
        header = new ArrayList<>();
    }

    public String get(int index){
        return header.get(index);
    }

    public List<String> header(){
        return Collections.unmodifiableList(header);
    }

    public int size(){
        return header.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("|");
        for(var each : header){
            if (each.isEmpty()) continue;
            sb.append(format("\t%s\t|", each));
        }
        return sb.toString();
    }

    public void clear(){
        header.clear();
    }

}
