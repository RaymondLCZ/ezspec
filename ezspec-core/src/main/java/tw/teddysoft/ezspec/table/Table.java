package tw.teddysoft.ezspec.table;

import java.util.*;

/**
 * {@code Table} is a class for representing examples and data tables.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Table {

    public static final  String TABLE_SEPARATOR = "|";
    private final Header header;
    private final List<Row> rows;
    private String rawData;

    public Table(){
        this.header = Header.create();
        this.rows = new LinkedList<>();
    }

    public Table(Table that){
        Objects.requireNonNull(that, "that");

        this.rawData = that.rawData;
        this.header = new Header(that.header);
        this.rows = new ArrayList<>(that.rows);
    }

    public Table(String rawData){
        Objects.requireNonNull(rawData, "rawData");

        String lines[] = rawData.split("\r?\n");

        List<String> cookedRawData =  Arrays.stream(lines)
                .filter(x-> x.contains("|"))
                .map(String::trim)
                .map( x-> x.substring(x.indexOf("|")))
                .filter( x -> x.startsWith("|"))
                .map(String::trim)
                .toList();

        List<String> headers = Arrays.stream(cookedRawData.get(0).split("\\|")).map(String::trim).filter(x ->! x.isEmpty()).toList();

        this.header = Header.valueOf(headers);
        rows = new ArrayList<>();
        for (int i = 1; i < cookedRawData.size(); i++){
            List<String> row = Arrays.stream(cookedRawData.get(i).split("\\|")).map(String::trim).filter(x ->! x.isEmpty()).toList();
            addRow(row);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(cookedRawData.get(0));
        for(int i = 1; i < cookedRawData.size(); i++){
            sb.append("\n").append(cookedRawData.get(i));
        }
        this.rawData = sb.toString();
    }

    public Table(Header header){
        this(header, new ArrayList<>());
    }

    public Table(Header header, List<Row> rows){
        this.header = header;
        this.rows = new LinkedList<>(rows);
    }

    public String getRawData() {
        return rawData;
    }

    public String get(String columnName){
        for(int i = 0; i < header.size(); i++){
            if (header.get(i).equals(columnName)){
                return rows.get(0).get(i);
            }
        }
        return "";
    }

    public String get(int columnIndex){
        return rows.get(0).get(columnIndex);
    }

    public Header header(){
        return header;
    }

    public List<Row> rows() {
        return Collections.unmodifiableList(rows);
    }

    public Row row(int index) {
        return rows.get(index);
    }

    public Row lastRow() {
        return rows.get(this.rows.size() - 1);
    }

    public Row row(String firstColumn) {
        for (var row : rows) {
            if (row.get(0).equals(firstColumn)) {
                return row;
            }
        }
        throw new RuntimeException("Row which first column '" + firstColumn + "' not found.");
    }

    public void addRow(List<String> columns) {
        List<String> newColumns = new ArrayList<>();
        for (String column : columns) {
            if (column.endsWith("\n")) {
                column = column.substring(0, column.length() - 1);
            }
            newColumns.add(column);
        }
        rows.add(new Row(header, newColumns, newColumns.size()));
    }

    public void addRow(Row row) {
        rows.add(row);
    }

    public void clear(){
        header.clear();
        rows.clear();
    }

    public static boolean containsTable(String description){
        String lines[] = description.split("\r?\n");

        return Arrays.stream(lines)
                .map(String::trim)
                .filter( x -> x.startsWith("|"))
                .findAny().isPresent();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(header.toString());
        rows.stream().forEach( row -> {
            sb.append("\n");
            sb.append("|");
            var rowText = row.toString();
            sb.append(rowText);
        });
        sb.append("\n");

        return sb.toString();
    }
}
