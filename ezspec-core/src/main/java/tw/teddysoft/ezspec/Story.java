package tw.teddysoft.ezspec;


import tw.teddysoft.ezspec.scenario.Background;
import tw.teddysoft.ezspec.scenario.RuntimeScenario;
import tw.teddysoft.ezspec.scenario.Scenario;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;
import tw.teddysoft.ezspec.visitor.SpecificationElement;
import tw.teddysoft.ezspec.visitor.SpecificationElementVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * {@code Story} is a class for representing a branch of the feature.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Story implements SpecificationElement {

    public static final String KEYWORD = "Story";
    private final String name;
    private final List<Scenario> scenarios;
    private String description;
    private int order;
    private Background background;

    /**
     * A static factory to create a story.
     *
     * @param name the name of story
     * @param order the order of story
     * @return a new story instance
     */
    public static Story New(String name, int order){
        Objects.requireNonNull(name, "name");

        return new Story(name, order);
    }

    /**
     * Instantiates a new Story.
     *
     * @param name  the name of story
     * @param order the order of story
     */
    public Story(String name, int order){
        this.name = name;
        this.order = order;
        description = "";
        scenarios = new ArrayList<>();
        background = Background.DEFAULT;
    }

    /**
     * Sets the description of story.
     *
     * @param description the description
     * @return the story
     */
    public Story description(String description){
        this.description = description;
        return this;
    }

    /**
     * A factory to create a scenario.
     *
     * @return the scenario
     */
    public Scenario newScenario() {
        return newScenario(Scenario.getEnclosingMethodName());
    }

    /**
     * Creates a scenario in story.
     *
     * @param name the name of scenario
     * @return the scenario
     */
    public Scenario newScenario(String name) {
        var existingScenario = scenarios.stream().filter(x -> x.getName().equals(name)).findFirst();
        if (existingScenario.isPresent()){
            return existingScenario.get();
        }

        var scenario = new RuntimeScenario(name, background);
        scenarios.add(scenario);
        return scenario;
    }

    /**
     * A factory to create a scenario outline.
     *
     * @return the scenario outline
     */
    public ScenarioOutline newScenarioOutline() {
        return newScenarioOutline(Scenario.getEnclosingMethodName(), "");
    }

    /**
     * A factory to create a scenario outline.
     *
     * @param name the name of scenario outline
     * @return the scenario outline
     */
    public ScenarioOutline newScenarioOutline(String name) {
        return newScenarioOutline(name, "");
    }

    /**
     * A factory to create a scenario outline.
     *
     * @param name        the name of scenario outline
     * @param description the description of scenario outline
     * @return the scenario outline
     */
    public ScenarioOutline newScenarioOutline(String name, String description) {
        var existingScenarioOutline = scenarios.stream().filter(x -> x.getName().equals(name)).findFirst();
        if (existingScenarioOutline.isPresent()){
            return (ScenarioOutline) existingScenarioOutline.get();
        }

        var scenarioOutline = new ScenarioOutline(name, description, background);
        scenarios.add(scenarioOutline);
        return scenarioOutline;
    }

    /**
     * Creates a background to story.
     *
     * @return the background
     */
    public Background newBackground() {
        background = new Background(Scenario.getEnclosingMethodName());
        return background;
    }

    /**
     * Creates a background to story.
     *
     * @param name the name of background
     * @return the background
     */
    public Background newBackground(String name) {
        background = new Background(name);
        return background;
    }

    /**
     * Gets scenarios.
     *
     * @return the scenarios
     */
    public List<Scenario> getScenarios() {
        return scenarios;
    }

    /**
     * Gets story name.
     *
     * @return the name of story
     */
    public String getName() {
        return name ;
    }

    /**
     * Gets story description.
     *
     * @return the description of story
     */
    public String description(){
        return description;
    }

    /**
     * Gets last scenario.
     *
     * @return the scenario
     */
    public Scenario lastScenario() {
        return scenarios.get(scenarios.size() - 1);
    }

    /**
     * Gets last scenario outline.
     *
     * @return the scenario outline
     */
    public ScenarioOutline lastScenarioOutline() {
        var scenarioOutlines = scenarios.stream().filter(x-> x instanceof ScenarioOutline).toList();
        return (ScenarioOutline) scenarioOutlines.get(scenarioOutlines.size() - 1);
    }

    /**
     * Gets order.
     *
     * @return the order
     */
    public int order(){
        return order;
    }

    /**
     * Story text string including story name, story description and background.
     *
     * @return the string
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!name.isEmpty())
            sb.append("Story: ").append(name).append("\n");
        if (null != description && !description.isEmpty())
            sb.append(description).append("\n");

        if (null != background && Background.DEFAULT != background){
            sb.append("Background: ").append(background.toString()).append("\n");
        }

        scenarios.forEach(scenario -> sb.append(scenario.toString()).append("\n"));
        return sb.toString();
    }

    /**
     * Null story.
     *
     * @return the null story
     */
    public static Story Null(){
        return new NullStory();
    }

    /**
     * Gets background.
     *
     * @return the background
     */
    public Background getBackground() {
        return background;
    }

    @Override
    public void accept(SpecificationElementVisitor visitor) {
        if (!name.isEmpty())
            visitor.visit(this);
        background.accept(visitor);
        scenarios.forEach(scenario -> {
            scenario.accept(visitor);
        });
    }

    /**
     * {@code NullStory} is a class for representing a null story.
     */
    public static class NullStory extends Story{

        private NullStory() {
            super("Null Story", -1);
        }
    }

}
