package tw.teddysoft.ezspec;


import tw.teddysoft.ezspec.scenario.Scenario;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;
import tw.teddysoft.ezspec.visitor.SpecificationElement;
import tw.teddysoft.ezspec.visitor.SpecificationElementVisitor;

import java.util.*;

import static tw.teddysoft.ezspec.scenario.Scenario.getEnclosingMethodName;

/**
 * {@code Feature} is a class for representing Gherkin feature file.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class Feature implements SpecificationElement {

    public static final int DEFAULT_STORY_ORDER = 0;
    public static final String KEYWORD = "Feature";
    private final String name;
    private final String description;
    private final List<Story> stories;
    private Story defaultStory;

    /**
     * A static factory to create a feature.
     *
     * @param name the name of feature
     * @return a new feature instance
     */
    public static Feature New(String name) {
        Objects.requireNonNull(name, "name");

        return new Feature(name);
    }

    /**
     * A static factory to create a feature.
     *
     * @param name        the name of feature
     * @param description the description of feature
     * @return a new feature instance
     */
    public static Feature New(String name, String description) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(description, "description");

        return new Feature(name, description);
    }

    private Feature(String name) {
        this(name, "");
    }

    /**
     * Instantiates a new Feature.
     *
     * @param name        the name
     * @param description the description
     */
    private Feature(String name, String description) {
        this.name = name;
        this.description = description;
        stories = new LinkedList<>();
    }

    /**
     * Gets stories.
     *
     * @return the stories
     */
    public List<Story> getStories() {
        return stories;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Create a new story in the feature.
     *
     * @param name the name of the story
     * @return the story
     */
    public Story newStory(String name) {
        if (null != defaultStory) throw new RuntimeException("require default story not exist");
        var story = new Story(name, stories.size() + 1);
        stories.add(story);
        return story;
    }


    /**
     * Create a new story in the feature.
     *
     * @param name  the name of the story
     * @param order the order of the story, starts from 1
     * @return the story
     */
    public Story newStory(String name, int order) {
        if (null != defaultStory) throw new RuntimeException("require default story not exist");
        if (order <= 0) throw new RuntimeException("require order greater than zero");

        var story = new Story(name, order);
        stories.add(story);
        return story;
    }

    /**
     * Create a new default story in the feature. A default story can only be
     * created when there is no any story in the list {@code stories}. If there
     * is a default story exist, it will be replaced by the new default story.
     *
     * @return the story
     */
    public Story newDefaultStory() {
        if (!stories.isEmpty()) throw new RuntimeException("require feature does not contain other story");
        defaultStory = new Story("", DEFAULT_STORY_ORDER);
        return defaultStory;
    }

    public Scenario newScenario() {
        return defaultStory.newScenario(getEnclosingMethodName());
    }

    public ScenarioOutline newScenarioOutline() {
        return defaultStory.newScenarioOutline(getEnclosingMethodName());
    }

    /**
     * Find the story with {@code storyName}.
     *
     * @param storyName the story name
     * @return the story
     */
    public Story withStory(String storyName) {
        return stories.stream().filter(x -> x.getName().equals(storyName)).findFirst().get();
    }

    /**
     * Find the story with {@code order}.
     *
     * @param order the story order
     * @return the story
     */
    public Story withStory(int order) {
        return stories.stream().filter(story -> story.order() == order).findFirst().get();
    }

    /**
     * Gets default story.
     *
     * @return the default story
     */
    public Story getDefaultStory() {
        return defaultStory;
    }

    /**
     * Clear story.
     */
    public void clearStory() {
        stories.clear();
    }

    @Override
    public void accept(SpecificationElementVisitor visitor) {
        visitor.visit(this);
        if (null != defaultStory) {
            defaultStory.accept(visitor);
        } else {
            stories.forEach(story -> {
                story.accept(visitor);
            });
        }
    }

    /**
     * Feature text string including feature name and feature description.
     *
     * @return the string
     */
    public String featureText() {
        StringBuilder sb = new StringBuilder();
        sb.append("Feature: ").append(name);

        if (!description.isEmpty())
            sb.append("\n\n").append(description);

        return sb.toString();
    }

    /**
     * The text string including feature name, feature description and
     * contents of all stories.
     *
     * @return the string
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(featureText());
        sb.append("\n\n");
        sb.append(storiesText());
        return sb.toString();
    }

    private String storiesText() {
        StringBuilder sb = new StringBuilder();
        if (stories.isEmpty()) {
            sb.append(defaultStory.toString()).append("\n");
        } else {
            var sortedStories = stories.stream().sorted(Comparator.comparing(Story::order)).toList();
            for (var each : sortedStories) {
                sb.append(each.toString()).append("\n");
            }
        }
        return sb.toString();
    }

}
