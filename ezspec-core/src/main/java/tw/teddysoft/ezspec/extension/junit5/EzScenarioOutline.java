package tw.teddysoft.ezspec.extension.junit5;


import org.apiguardian.api.API;
import org.junit.jupiter.api.Test;

import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@API(
        status = API.Status.EXPERIMENTAL,
        since = "1.0"
)
@Test
public @interface EzScenarioOutline {
}
