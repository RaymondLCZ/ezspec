package tw.teddysoft.ezspec.extension;

/**
 * {@code SpecUtils} is a class for parsing values to JUnit report.
 *
 * @author Teddy Chen
 * @since 1.0
 */
public class SpecUtils {

    /**
     * Replaces underscores with spaces.
     *
     * @param name the name of test case
     * @return the replaced name of test case
     */
    public static String getReplacedUnderscores(String name) {
        return name.replace("_", " ");
    }

    /**
     * Centers strings in table.
     *
     * @param str   the str
     * @param width the column width
     * @return the string
     */
    public static String center(String str, int width){
        int padding = (width - str.length()) / 2;
        if (0 == padding) return str;

        String centeredStr = String.format("%" + padding + "s%s%" + padding + "s%n", "", str, "");
        return deleteEndWithNewLine(centeredStr);
    }

    public static String deleteEndWithNewLine(String str){
        return str.replaceAll("[\\n\\r]+$", "");
    }

}
