package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;

@EzFeature
public class StorySpec {
    static final String FEATURE_NAME = "Create Card";
    static Feature feature = Feature.New(FEATURE_NAME);

    @BeforeAll
    static void beforeAll() {
        feature.clearStory();
        feature.newStory("Team staffs create a card to represent a work item", 10)
                .description("""
                        In order to manage my work
                        As a team staff
                        I want to create a card""");
    }

    @Test
    public void verify_story_content(){

        String expectedName = "Team staffs create a card to represent a work item";
        assertEquals(feature.withStory(expectedName), feature.withStory(10));

        Story story = feature.withStory(expectedName);
        assertEquals(expectedName, story.getName());

        String expectedDescription = """
                        In order to manage my work
                        As a team staff
                        I want to create a card""";
        assertEquals(expectedDescription, story.description());

        assertEquals(format("Story: %s\n%s", story.getName(), story.description()),
                story.toString().trim());

        assertEquals(0, story.getScenarios().size());
    }
}
