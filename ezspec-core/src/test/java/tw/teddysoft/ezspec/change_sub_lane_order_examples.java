package tw.teddysoft.ezspec;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import tw.teddysoft.ezspec.extension.junit5.Junit5Examples;
import tw.teddysoft.ezspec.table.Table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static tw.teddysoft.ezspec.extension.junit5.JunitUtils.constructTable;

public class change_sub_lane_order_examples extends Junit5Examples {
    public static final List<String> HEADER = new ArrayList<>(Arrays.asList("example_code", "lane_name", "new_parent_name", "new_position", "event_count", "given_workflow", "expected_workflow"));
    public static final String WORKFLOW = "Workflow";

    @Override
    public String getDescription() {
        return "The order of sub lanes can be changed.";
    }

    @Override
    protected String getExamplesRawData() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Table getTable() {
        return constructTable(HEADER, provideArguments(null));
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        String givenWorkflow = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubStageToMiddle = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubStageToLast = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubStageToFirst = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubStageToSamePosition = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubSwimlaneToMiddle = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubSwimlaneToLast = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubSwimlaneToFirst = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        String expectedSubSwimlaneToSamePosition = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Team B        | team_b       |    doing      |   Swimlane   |   -1       |
                | Team C        | team_c       |    doing      |   Swimlane   |   -1       |
                """;

        return Stream.of(
//                Arguments.of("ML-E01", "Step 1", "To Do", "1", 1),
//                Arguments.of("ML-E02", "Step 1", "To Do", "2", 1),
//                Arguments.of("ML-E03", "Step 3", "To Do", "0", 1),
//                Arguments.of("ML-E04", "Step 1", "To Do", "0", 0),
//                Arguments.of("ML-E05", "Team A", "Doing", "1", 1),
                Arguments.of("ML-E06", "Team B", "Doing", "2", 1, givenWorkflow, expectedSubSwimlaneToLast),
                Arguments.of("ML-E07", "Team B", "Doing", "0", 1, givenWorkflow, expectedSubSwimlaneToFirst),
                Arguments.of("ML-E08", "Team A", "Doing", "0", 0, givenWorkflow, expectedSubSwimlaneToSamePosition)
        );
    }
}
