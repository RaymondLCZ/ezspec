package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.scenario.Scenario;
import tw.teddysoft.ezspec.table.Table;

import static org.junit.jupiter.api.Assertions.assertEquals;

@EzFeature
public class TableSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Table");
    }

    @Nested
    @Order(1)
    class UnitTest {
        final String table_with_clean_data = """
            |    owner          |   point-s      |   status Points  |
            |    Jill           |   $100,000     |   80 0           |
            |    Teddy Chen     |   50000        |   50.0           |""";

        final String table_with_dirty_prefix_string = """
            Given the following table:   |    owner          |   point-s      |   status Points  |
                                         |    Jill           |   $100,000     |   80 0           |
                                         |    Teddy Chen     |   50000        |   50.0           |
                                         """;

        final String table_with_dirty_postfix_string = """
            |    owner          |   point-s      |   status Points  |
            |    Jill           |   $100,000     |   80 0           |
            |    Teddy Chen     |   50000        |   50.0           |
             This is a noise string
            """;

        @ParameterizedTest
        @ValueSource(strings = {table_with_clean_data, table_with_dirty_prefix_string, table_with_dirty_postfix_string})
        public void creating_tables_with_clean_and_dirty_data(String tableData){

            Table table = new Table(tableData);

            assertEquals(3, table.header().size());
            assertEquals(2, table.rows().size());

            assertEquals("owner", table.header().get(0));
            assertEquals("point-s", table.header().get(1));
            assertEquals("status Points", table.header().get(2));

            assertEquals("Jill", table.row(0).get(0));
            assertEquals("$100,000", table.row(0).get(1));
            assertEquals("80 0", table.row(0).get(2));

            assertEquals("Teddy Chen", table.row(1).get(0));
            assertEquals("50000", table.row(1).get(1));
            assertEquals("50.0", table.row(1).get(2));

            assertEquals(table_with_clean_data, table.getRawData());
        }
  }

    @Nested
    @Order(2)
    class ReadAnonymousTable {
        static Story readAnonymousTable;

        @BeforeAll
        public static void beforeAll() {
            readAnonymousTable = feature.newStory("Different ways to read table in a scenario")
                    .description("""
                            In order to provide multiple data when specifying a step 
                            As a bdd participant
                            I want to specify tables in a scenario
                            """);
        }

        @EzScenario
        public void read_anonymous_table_in_Given$comma$_When$comma$_Then$comma$_as_well_as_And() {
            Scenario.New(readAnonymousTable)
                    .Given("""
                            the following table:
                            |    owner   |   points      |   statusPoints  |
                            |    Jill    |   100,000     |   800           |
                            |    Jow     |   50,000      |   50            |""", env -> {
                        assertEquals("Jill", env.row(0).get("owner"));
                        assertEquals("100,000", env.row(0).get("points"));
                        assertEquals("800", env.row(0).get("statusPoints"));
                        var x = 1;
                    })
                    .When("I execute the scenario", env -> {
                        assertEquals("Jow", env.row("Jow").get("owner"));
                        assertEquals("50,000", env.row("Jow").get("points"));
                        assertEquals("50", env.row("Jow").get("statusPoints"));
                    })
                    .Then("I can read env in 'Given', 'When', 'Then'", env -> {
                        assertEquals("Jill", env.row("Jill").get("owner"));
                        assertEquals("100,000", env.row(0).get("points"));
                        assertEquals("800", env.row(0).get("statusPoints"));
                    }).
                    And("'And' clauses", env -> {
                        assertEquals(env.row("Jill").get("statusPoints"), env.row(0).get("statusPoints"));
                        assertEquals(env.row("Jow").get("statusPoints"), env.row(1).get("statusPoints"));
                    })
                    .Execute();
        }


        @EzScenario
        public void read_anonymous_table_via_table$dot$row$parenthesis$() {
            Scenario.New(readAnonymousTable)
                    .Given("""
                            the following table:
                            |    owner   |   points      |   statusPoints  |
                            |    Jill    |   100,000     |   800           |
                            |    Jow     |   50,000      |   50            |""", env -> {
                    })
                    .When("I execute the scenario", env -> {
                    })
                    .Then("""
                            I can get each column of the first row by index via 'table.row("Jill").get(0), table.row("Jill").get(1), table.row("Jill").get(2)'""", env -> {
                        assertEquals("Jill", env.row("Jill").get(0));
                        assertEquals("100,000", env.row("Jill").get(1));
                        assertEquals("800", env.row("Jill").get(2));
                    }).And("""
                            via 'table.row("Jow").get("owner"), table.row("Jow").get("points"), table.row("Jow").get("points")' to get each column of the second row""", env -> {
                        assertEquals("Jow", env.row("Jow").get("owner"));
                        assertEquals("50,000", env.row("Jow").get("points"));
                        assertEquals("50", env.row("Jow").get("statusPoints"));
                    }).Execute();
        }
    }

    @Nested
    @Order(100)
    class UsageExample{
        static Story usageExample;

        @BeforeAll
        public static void beforeAll() {
            usageExample = feature.newStory("Show how to use Table in BDD", 1)
                    .description("""
                In order to demonstrate the purpose of Table in BDD 
                As an ezBehave developer
                I want show an example
                """);
        }

        @EzScenario
        public void two_tables_in_a_scenario_taken_from_BDD_In_Action() {
            Scenario.New(usageExample)
                    .Given("""
                       the following accounts:
                       |    owner   |   points      |   statusPoints  |
                       |    Jill    |   100,000     |   800           |
                       |    Jow     |   50,000      |   50            |""", env -> {
                        assertEquals(env.row(0).get("owner"), env.row(0).get(0));
                        assertEquals(env.row("Jow").get("statusPoints"), env.row(1).get(2));
                        assertEquals(env.row("Jow").get(2), env.row(1).get(2));
                    })
                    .When("Martin Joe transfers 4000 points to Jill", env -> {
                        assertEquals("100,000", env.row("Jill").get("points"));
                    })
                    .Then("""
                   the accounts should be the following:
                   |    owner   |   points      |   statusPoints  |
                   |    Jill    |   140,000     |   800           |
                   |    Jow     |   10,000      |   50            |
                    """, env -> {
                        assertEquals("140,000", env.row("Jill").get("points"));
                        assertEquals("10,000", env.row("Jow").get(1));
                    }).Execute();
        }
    }

    @Test
    public void table_clear() {
        Table table = new Table("|    owner   |   points      |   statusPoints  |\n" +
                "                       |    Jill    |   100,000     |   800           |\n" +
                "                       |    Jow     |   50,000      |   50            |");
        table.clear();

        assertEquals(0, table.header().size());
        assertEquals(0, table.rows().size());
    }

}
