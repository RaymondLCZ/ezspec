package tw.teddysoft.ezspec;


import org.junit.jupiter.api.BeforeAll;
import tw.teddysoft.ezspec.example.Example;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;
import tw.teddysoft.ezspec.extension.junit5.Junit5Examples;
import tw.teddysoft.ezspec.scenario.ScenarioOutline;

import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@EzFeature
public class ScenarioOutlineSpec {
    static Feature feature;
    static Story storyHavingScenarioOutlines;
    private final PrintStream originalOut = System.out;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("ScenarioOutline is a Scenario with Examples");
        storyHavingScenarioOutlines = feature.newStory("Scenario with examples")
                .description("""
                    In order to verify a scenario with different data sets
                    As a bdd participant
                    I want to specify examples in a scenario""");
    }

    @EzScenarioOutline
    public void scenario_outline_full_text(){
        var scenarioOutlineFullText = feature.newStory("A scenario outline with description", 1).
                newScenarioOutline("scenario outline")
                .WithExamples(Junit5Examples.get(move_sub_lane_under_lane_examples.class))
                .toString();

        var expectedResult = """
                Scenario outline: scenario outline
                                
                                
                Examples: Move sub lane under lane
                A sub lane can be moved under another lane.
                |	example_code	|	lane_name	|	new_parent_name	|	new_position	|	event_count	|	given_workflow	|	expected_workflow	|
                |     ML-B01     	|   Step 1   	|      Step 3      	|       0       	|       1       	|
                <given_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                <expected_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Design        | design       |    to_do      |   Stage      |   4        |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                |     ML-B02     	|   Step 2   	|      Feature      	|       0       	|       1       	|
                <given_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                <expected_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Feature       | feature      |    step_2     |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                |     ML-B03     	|   Design   	|       To Do       	|       3       	|       1       	|
                <given_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                <expected_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                | Team A        | team_a       |    feature    |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                |     ML-B04     	|   Feature   	|      Step 2      	|       1       	|       1       	|
                <given_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                <expected_workflow>
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                | To Do         | to_do        |    -1         |   Stage      |   3        |
                | Doing         | doing        |    -1         |   Stage      |   5        |
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                """;
        assertEquals(expectedResult, scenarioOutlineFullText);
    }

    @EzScenarioOutline
    public void scenario_outline_with_description() {
        final String examples = """
                | used      | debt        | score     |   result  |
                | 40%       | $202,704    | 499       |   reject  |
                | 41%       | $202,704    | 500       |   accept  |
                """;

        var scenarioWithDescription = feature.newStory("A scenario outline with description", 1)
                .description("""
                    In order to explain a scenario outline
                    As a bdd participant
                    I want to specify description in a scenario outline
                    """);

        ScenarioOutline.New("Screening tenant leads based on credit score", """
                        TENANTS PIPELINE is a list of verified tenant leads a landlord can choose from.
                        
                        Credit score is calculated by an external auditor
                        through their API and has a range of 300-850.
                        """, scenarioWithDescription)
                .WithExamples(examples)
                .Given("a tenant lead Simona Jenkins with credit used <used> and total debt <debt>:", env -> {
                })
                .And("that the lead waits in the queue to the tenants pipeline", env -> {})
                .When("the tenant lead has a <score>", env -> {
                })
                .Then("we should <result> the lead", env -> {
                }).Execute();

        String expected = """
                Story: A scenario outline with description
                In order to explain a scenario outline
                As a bdd participant
                I want to specify description in a scenario outline

                Scenario outline: Screening tenant leads based on credit score

                TENANTS PIPELINE is a list of verified tenant leads a landlord can choose from.

                Credit score is calculated by an external auditor
                through their API and has a range of 300-850.

                Given a tenant lead Simona Jenkins with credit used <used> and total debt <debt>:
                And that the lead waits in the queue to the tenants pipeline
                When the tenant lead has a <score>
                Then we should <result> the lead

                Examples:\s
                |	used	|	debt	|	score	|	result	|
                |  40%  	|$202,704	|   499   	|  reject  	|
                |  41%  	|$202,704	|   500   	|  accept  	|

                """;

        assertEquals(expected, scenarioWithDescription.toString());
    }

    @EzScenarioOutline
    public void scenario_outline_with_two_examples() {
        final String tableTooShort = """
                | Password  | Valid or Invalid  |
                | abc       | invalid           |
                | ad2       | invalid           |
                """;
        Example tooShort = new Example("Too Short",
                "Passwords are invalid if less than 4 characters",
                tableTooShort);

        final String tableLettersAndNumbers = """
                | Password  | Valid or Invalid  |
                | abc1      | valid             |
                | adcd      | invalid           |
                | adcd1     | valid             |
                """;
        Example lettersAndNumbers = new Example("Letters and Numbers",
                "Passwords need both letters and numbers to be valid",
                tableLettersAndNumbers);

        var scenarioOutline = feature.withStory(storyHavingScenarioOutlines.getName()).
                newScenarioOutline()
                .WithExamples(tooShort, lettersAndNumbers);

        scenarioOutline.Given("I try to create an account with password <Password>", true, env -> {
                    assertEquals(env.getInput().get("Password"), env.gets("Password"));
                })
                .Then("I should see that the password is <Valid or Invalid>", env -> {
                    assertEquals(env.getInput().get("Valid or Invalid"), env.gets("Valid or Invalid"));
                })
                .Execute();

        assertEquals("I try to create an account with password <abc>", scenarioOutline.RuntimeScenarios().get(0).steps().get(0).description());
        assertEquals("I should see that the password is <invalid>", scenarioOutline.RuntimeScenarios().get(0).steps().get(1).description());
        assertEquals("I try to create an account with password <ad2>", scenarioOutline.RuntimeScenarios().get(1).steps().get(0).description());
        assertEquals("I should see that the password is <invalid>", scenarioOutline.RuntimeScenarios().get(1).steps().get(1).description());
        assertEquals("I try to create an account with password <abc1>", scenarioOutline.RuntimeScenarios().get(2).steps().get(0).description());
        assertEquals("I should see that the password is <valid>", scenarioOutline.RuntimeScenarios().get(2).steps().get(1).description());
        assertEquals("I try to create an account with password <adcd>", scenarioOutline.RuntimeScenarios().get(3).steps().get(0).description());
        assertEquals("I should see that the password is <invalid>", scenarioOutline.RuntimeScenarios().get(3).steps().get(1).description());
        assertEquals("I try to create an account with password <adcd1>", scenarioOutline.RuntimeScenarios().get(4).steps().get(0).description());
        assertEquals("I should see that the password is <valid>", scenarioOutline.RuntimeScenarios().get(4).steps().get(1).description());

        var expected = """
                Scenario outline: scenario outline with two examples

                Given I try to create an account with password <Password>
                Then I should see that the password is <Valid or Invalid>

                Examples: Too Short
                Passwords are invalid if less than 4 characters
                |	Password	|	Valid or Invalid	|
                |    abc    	|      invalid      	|
                |    ad2    	|      invalid      	|

                Examples: Letters and Numbers
                Passwords need both letters and numbers to be valid
                |	Password	|	Valid or Invalid	|
                |    abc1    	|       valid       	|
                |    adcd    	|      invalid      	|
                |   adcd1   	|       valid       	|
                """;
        assertEquals(expected, storyHavingScenarioOutlines.lastScenarioOutline().toString());
    }

    @EzScenarioOutline
    public void read_data_from_examples_using_correct_table_column_name_and_index() {
        final String examples = """
             |  tax_included    | VAT   | tax_excluded | notes         |
             |  99              | 5     | 94           | 四捨五入案例    |
             |  0               | 0     | 0            | 可以開零元發票   |
             |  1               | 0     | 1            | 邊界條件        | 
             |  10              | 0     | 10           | 邊界條件        | 
             |  11              | 1     | 10           | 邊界條件        | 
            """;

        final String expectedResult [][] = {
                {"tax_included", "VAT", "tax_excluded", "notes"},
                {"99", "5", "94", "四捨五入案例"},
                {"0", "0", "0", "可以開零元發票"},
                {"1", "0", "1", "邊界條件"},
                {"10", "0", "10", "邊界條件"},
                {"11", "1", "10", "邊界條件"}
        };

        ScenarioOutline.New(storyHavingScenarioOutlines)
                .WithExamples(examples)
                .Given("a scenario", env -> {
                })
                .And("examples with data set of columns <tax_included>, <VAT>, <tax_excluded>, <notes>", env -> {
                    assertEquals(expectedResult[0][0], env.getInput().header().get(0));
                    assertEquals(expectedResult[0][1], env.getInput().header().get(1));
                    assertEquals(expectedResult[0][2], env.getInput().header().get(2));
                    assertEquals(expectedResult[0][3], env.getInput().header().get(3));
                })
                .When("I run the scenario", env -> { })
                .Then("I can read data from the data set with column names", env -> {
                    assertEquals(expectedResult[env.getExecutionCount()][0], env.getInput().get("tax_included"));
                    assertEquals(expectedResult[env.getExecutionCount()][1], env.getInput().get("VAT"));
                    assertEquals(expectedResult[env.getExecutionCount()][2], env.getInput().get("tax_excluded"));
                    assertEquals(expectedResult[env.getExecutionCount()][3], env.getInput().get("notes"));
                    })
                .And("with column indexes", env -> {
                    assertEquals(expectedResult[env.getExecutionCount()][0], env.getInput().get(0));
                    assertEquals(expectedResult[env.getExecutionCount()][1], env.getInput().get(1));
                    assertEquals(expectedResult[env.getExecutionCount()][2], env.getInput().get(2));
                    assertEquals(expectedResult[env.getExecutionCount()][3], env.getInput().get(3));
                }).Execute();
    }

    @EzScenarioOutline
    public void cannot_read_data_from_examples_using_incorrect_keys() {
        String examples = """
                  | tax_included    | VAT   | tax_excluded | notes
                  | 99              | 5     | 94           | 四捨五入案例
                  | 0               | 0     | 0            | 可以開零元發票
                  | 1               | 0     | 1            | 邊界條件
                  | 10              | 0     | 10           | 邊界條件
                  | 11              | 1     | 10           | 邊界條件
            """;

        ScenarioOutline.New(storyHavingScenarioOutlines)
                .WithExamples(examples)
                .Given("a scenario", env -> {
                })
                .And("examples with data set of columns <text_included>, <VAT>, <tax_excluded>, <notes>", env -> {})
                .When("I run the scenario", env -> {
                })
                .Then("I cannot read data with incorrect column names <note>, <NOTES>, <vAT>", env -> {
                    assertTrue(env.gets("note").isEmpty());
                    assertTrue(env.gets("NOTES").isEmpty());
                    assertTrue(env.gets("vAT").isEmpty());
                }).Execute();
    }
}
