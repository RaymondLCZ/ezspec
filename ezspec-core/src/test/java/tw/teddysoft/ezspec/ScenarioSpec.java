package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.scenario.Scenario;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.ezspec.exception.PendingException.pending;
import static tw.teddysoft.ezspec.scenario.Step.ContinuousAfterFailure;
import static tw.teddysoft.ezspec.scenario.Step.TerminateAfterFailure;

@EzFeature
public class ScenarioSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Scenario");
    }

    @Nested
    @Order(1)
    class SimpleScenarioBehavior {
        static Story scenarioBehavior;

        @BeforeAll
        public static void beforeAll() {
            scenarioBehavior = feature.newStory("Simple scenario")
                    .description("""
                            In order to clarify specifications
                            As a bdd participant
                            I want to specify scenarios""");
        }

        @EzScenario
        public void scenario_using_then_success_without_description() {
            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(scenarioBehavior)
                    .Given("the tax excluded price of a computer is $20,000", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("the VAT rate is 5%", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .When("I buy the computer", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenSuccess(TerminateAfterFailure, env -> {
                        stack.push(counter.getAndIncrement());
                    }).Execute();
        }

        @EzScenario
        public void scenario_using_then_success_without_description_and_continuous() {
            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(scenarioBehavior)
                    .Given("the tax excluded price of a computer is $20,000", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("the VAT rate is 5%", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .When("I buy the computer", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenSuccess(e -> {
                        stack.push(counter.getAndIncrement());
                    }).Execute();
        }

        @EzScenario
        public void scenario_using_then_failure_without_description() {
            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(scenarioBehavior)
                    .Given("the tax excluded price of a computer is $20,000", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("the VAT rate is 5%", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .When("I buy the computer", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenFailure(TerminateAfterFailure, e -> {
                        stack.push(counter.getAndIncrement());
                    }).Execute();
        }

        @EzScenario
        public void essential_scenario_with_Given_And_When_Then() {
            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(scenarioBehavior)
                    .Given("the tax excluded price of a computer is $20,000", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("the VAT rate is 5%", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .When("I buy the computer", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .Then("I need to pay $21,000", env -> {
                        stack.push(counter.getAndIncrement());
                    }).Execute();

            assertEquals(4, stack.size());
            for (int i = stack.size() - 1; i >= 0; i--) {
                assertEquals(i, stack.pop());
            }

            String expectedFullText = """
                    Scenario: essential scenario with Given And When Then
                    Given the tax excluded price of a computer is 20,000
                    And the VAT rate is 5%
                    When I buy the computer
                    Then I need to pay 21,000
                        """;
            assertEquals(expectedFullText, scenarioBehavior.lastScenario().toString());
        }

        @EzScenario
        public void a_scenario_with_only_And_is_correct() {
            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(scenarioBehavior)
                    .And("the VAT rate is 5%", env -> {
                        stack.push(counter.getAndIncrement());
                    }).Execute();

            assertEquals(1, stack.size());
            for (int i = stack.size() - 1; i >= 0; i--) {
                assertEquals(i, stack.pop());
            }

            String expectedFullText = """
                        Scenario: a scenario with only And is correct
                        And the VAT rate is 5%
                        """;
            assertEquals(expectedFullText, scenarioBehavior.lastScenario().toString());
        }

        @EzScenario
        public void a_sequential_scenario_with_pending() {

            var scenario = Scenario.New(scenarioBehavior);
            scenario
                    .Given("a pending Given without pending reasons", env -> {
                        pending();
                    })
                    .And("a pending And with a pending reason", env -> {
                        pending("Waiting for verification");
                    })
                    .When("a pending When is executed without pending reasons", env -> {
                        pending();
                    })
                    .Then("a pending Then should be displayed with a pending reason", env -> {
                        pending("This is a pending in the Then");
                    })
                    .And("a pending And should be displayed without pending reasons", env -> {
                        pending();
                    })
                    .Execute();

            String expectedFullText = """
                Scenario: a sequential scenario with pending
                Given a pending Given without pending reasons
                And a pending And with a pending reason
                When a pending When is executed without pending reasons
                Then a pending Then should be displayed with a pending reason
                And a pending And should be displayed without pending reasons
                        """;
            assertEquals(expectedFullText, scenarioBehavior.lastScenario().toString());

            scenario.steps().forEach(step -> {
                assertTrue(step.getResult().isPending());
            });
        }

        @EzScenario
        public void a_concurrent_scenario_with_pending() {

            var scenario = Scenario.New(scenarioBehavior);

            scenario.New(scenarioBehavior)
                    .Given("a pending Given without pending reasons", env -> {
                        pending();
                    })
                    .And("a pending And with a pending reason", env -> {
                        pending("Waiting for verification");
                    })
                    .When("a pending When is executed without pending reasons", env -> {
                        pending();
                    })
                    .Then("a pending Then should be displayed with a pending reason", env -> {
                        pending("This is a pending in the Then");
                    })
                    .And("a pending And should be displayed without pending reasons", env -> {
                        pending();
                    })
                    .ExecuteConcurrently();

            String expectedFullText = """
                Scenario: a concurrent scenario with pending
                Given a pending Given without pending reasons
                And a pending And with a pending reason
                When a pending When is executed without pending reasons
                Then a pending Then should be displayed with a pending reason
                And a pending And should be displayed without pending reasons
                        """;
            assertEquals(expectedFullText, scenarioBehavior.lastScenario().toString());

            scenario.steps().forEach(step -> {
                assertTrue(step.getResult().isPending());
            });
        }

    }


    @Nested
    @Order(10)
    class DuplicatedStepBehavior{
        static Story duplicationSteps;

        @BeforeAll
        public static void beforeAll() {
            duplicationSteps = feature.newStory("Duplication steps", 2)
                    .description("""
                    In order to avoid confusion when duplicated steps such as Given, When, Then are specified
                    As a bdd participant
                    I want to execute the first one duplicated step only""");
        }

        @EzScenario
        public void two_exactly_the_same_Given() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("two same Given in a scenario", env -> {
                        stack.push("first");
                    })
                    .Given("two same Given in a scenario", env ->{
                        stack.push("second");
                    })
                    .When("I execute the scenario", env ->{})
                    .Then("both Given are executed", env ->{})
                    .Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());
            String expectedFullText = """
                    Scenario: two exactly the same Given
                    Given two same Given in a scenario
                    Given two same Given in a scenario
                    When I execute the scenario
                    Then both Given are executed
                    """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }

        @EzScenario
        public void two_exactly_the_same_When() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("two same When in a scenario", env -> {})
                    .When("I execute the scenario", env ->{
                        stack.push("first");
                    })
                    .When("I execute the scenario", env ->{
                        stack.push("second");
                    })
                    .Then("both When are executed", env ->{})
                    .Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());

            String expectedFullText = """
                Scenario: two exactly the same When
                Given two same When in a scenario
                When I execute the scenario
                When I execute the scenario
                Then both When are executed
                    """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }

        @EzScenario
        public void two_exactly_the_same_Then() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("two same Then in a scenario", env -> {})
                    .When("I execute the scenario", env ->{
                    })
                    .Then("both Then are executed", env ->{
                        stack.push("first");
                    })
                    .Then("both Then are executed", env -> {
                        stack.push("second");
                    }).Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());

            String expectedFullText = """
                Scenario: two exactly the same Then
                Given two same Then in a scenario
                When I execute the scenario
                Then both Then are executed
                Then both Then are executed
                    """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }

        @EzScenario
        public void two_exactly_the_same_And() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("a scenario", env -> {})
                    .And("two same And in a scenario", env ->{
                        stack.push("first");
                    })
                    .And("two same And in a scenario", env ->{
                        stack.push("second");
                    })
                    .When("I execute the scenario", env ->{
                    })
                    .Then("both And are executed", env -> {})
                    .Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());

            String expectedFullText = """
                Scenario: two exactly the same And
                Given a scenario
                And two same And in a scenario
                And two same And in a scenario
                When I execute the scenario
                Then both And are executed
                """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }

        @EzScenario
        public void two_exactly_the_same_But() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("a scenario", env -> {})
                    .But("two same But in a scenario", env ->{
                        stack.push("first");
                    })
                    .But("two same But in a scenario", env ->{
                        stack.push("second");
                    })
                    .When("I execute the scenario", env ->{
                    })
                    .Then("both But are executed", env -> {})
                    .Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());

            String expectedFullText = """
                    Scenario: two exactly the same But
                    Given a scenario
                    But two same But in a scenario
                    But two same But in a scenario
                    When I execute the scenario
                    Then both But are executed
                    """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }

        @EzScenario
        public void two_exactly_the_same_ThenSuccess() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("two same ThenSuccess in a scenario", env -> {})
                    .When("I execute the scenario", env ->{
                    })
                    .ThenSuccess("both ThenSuccess are executed", env -> {
                        stack.push("first");
                    })
                    .ThenSuccess("both ThenSuccess are executed", env -> {
                        stack.push("second");
                    }).Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());

            String expectedFullText = """
                    Scenario: two exactly the same ThenSuccess
                    Given two same ThenSuccess in a scenario
                    When I execute the scenario
                    Then success both ThenSuccess are executed
                    Then success both ThenSuccess are executed
                    """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }

        @EzScenario
        public void two_exactly_the_same_ThenFailure() {
            final Stack<String> stack = new Stack<>();

            Scenario.New(duplicationSteps)
                    .Given("two same ThenFailure in a scenario", env -> {})
                    .When("I execute the scenario", env ->{
                    })
                    .ThenFailure("both ThenFailure are executed", env -> {
                        stack.push("first");
                    })
                    .ThenFailure("both ThenFailure are executed", env -> {
                        stack.push("second");
                    }).Execute();

            assertEquals(2, stack.size());
            assertEquals("second", stack.pop());
            assertEquals("first", stack.pop());

            String expectedFullText = """
                    Scenario: two exactly the same ThenFailure
                    Given two same ThenFailure in a scenario
                    When I execute the scenario
                    Then failure both ThenFailure are executed
                    Then failure both ThenFailure are executed
                    """;
            assertEquals(expectedFullText, duplicationSteps.lastScenario().toString());
        }
    }


    @Nested
    @Order(30)
    class FailureScenarioBehavior {
        static Story scenarioBehavior;

        @BeforeAll
        public static void beforeAll() {
            scenarioBehavior = feature.newStory("Exceptional scenario")
                    .description("""
                            In order to understand the correctness of specifications
                            As a bdd participant
                            I want to see the execution results of specifications""");
        }

        @Test
        public void default_failure_scenario() {

            var scenario = Scenario.New(scenarioBehavior);
            var exception =
                    assertThrows(Throwable.class, () -> {
                        scenario
                                .Given("a failed given", env -> {
                                    fail("Given failed and terminated the execution");
                                })
                                .When("I execute a failed when", env -> {
                                    fail("Unreachable path");
                                })
                                .Then("a failed then is executed", env -> {
                                    fail("Unreachable path");
                                })
                                .Execute();
                    });

            assertTrue(scenario.steps().get(0).getResult().isFailure());
            assertTrue(scenario.steps().get(1).getResult().isSkipped());
            assertTrue(scenario.steps().get(2).getResult().isSkipped());

            assertEquals("Given failed and terminated the execution", exception.getMessage());
        }

        @Test
        public void continue_when_failure_scenario_with_Given_And_When_Then() {

            var scenario = Scenario.New(scenarioBehavior);
            var exception =
            assertThrows(Throwable.class, () -> {
                scenario
                        .Given("a failed given", ContinuousAfterFailure, env -> {
                            fail("Given failed and continuously execute");
                        })
                        .When("I execute a failed when", ContinuousAfterFailure, env -> {
                            fail("When failed and continuously execute");
                        })
                        .Then("a failed then is executed", ContinuousAfterFailure, env -> {
                            fail("Then failed and continuously execute");
                        })
                        .Execute();
            });

            scenario.steps().forEach(step -> {
                assertTrue(step.getResult().isFailure());
            });

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter(x-> !(x.trim().isEmpty())).toList();
            assertEquals(3, errors.size());
        }
    }

    @Nested
    @Order(40)
    class ConcurrentScenarioBehavior {
        static Story scenarioBehavior;

        @BeforeAll
        public static void beforeAll() {
            scenarioBehavior = feature.newStory("Concurrent scenario")
                    .description("""
                            In order to specify concurrent specifications
                            As a bdd participant
                            I want to execute a scenario concurrently""");
        }

        @Test
        public void a_concurrent_scenario_with_ContinueWhenFailure() {

            var scenario = Scenario.New(scenarioBehavior);
            var exception = assertThrows(Throwable.class, () -> {
                scenario
                        .Given("a failed given with [continue when failure] setting", ContinuousAfterFailure, env -> {
                            assertTrue(false);
                        })
                        .When("I unsuccessfully execute the scenario concurrently ", ContinuousAfterFailure, env -> {
                            assertTrue(false);
                        })
                        .Then("the Then is correct", ContinuousAfterFailure, env -> {
                            assertTrue(true);
                        })
                        .And("the first And fails", ContinuousAfterFailure, env -> {
                            sleep(100);
                            assertTrue(false);
                        })
                        .And("the second And is success", ContinuousAfterFailure, env -> {
                            sleep(50);
                            assertEquals("A", "A");
                        }).ExecuteConcurrently();
            });

            assertTrue(scenario.steps().get(0).getResult().isFailure());
            assertTrue(scenario.steps().get(1).getResult().isFailure());
            assertTrue(scenario.steps().get(2).getResult().isSuccess());
            assertTrue(scenario.steps().get(3).getResult().isFailure());
            assertTrue(scenario.steps().get(4).getResult().isSuccess());

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter( x-> !(x.trim().isEmpty())).toList();
            assertEquals(3, errors.size());
        }

        @Test
        public void a_concurrent_scenario_failing_on_Given_without_ContinueWhenFailure() {
            var scenario = Scenario.New(scenarioBehavior);

            var exception = assertThrows(Throwable.class, () -> {
                scenario
                        .Given("a failed Given without [continue when failure] setting (i.e., the default mode is [terminate when failure]", env -> {
                            assertEquals("The Given succeeded", "The Given failed");
                        })
                        .When("I unsuccessfully execute the scenario concurrently ", env -> {
                            fail("Unreachable path");
                        })
                        .Then("the Then is not executed", env -> {
                            fail("Unreachable path");
                        })
                        .And("the first And is not executed", env -> {
                            sleep(100);
                            fail("Unreachable path");
                        })
                        .And("the second And is not executed", env -> {
                            sleep(50);
                            fail("Unreachable path");
                        }).ExecuteConcurrently();
            });

            assertTrue(scenario.steps().get(0).getResult().isFailure());
            assertTrue(scenario.steps().get(1).getResult().isSkipped());
            assertTrue(scenario.steps().get(2).getResult().isSkipped());
            assertTrue(scenario.steps().get(3).getResult().isSkipped());
            assertTrue(scenario.steps().get(4).getResult().isSkipped());

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter( x-> !(x.trim().isEmpty())).toList();
            assertEquals(1, errors.size());
        }


        @Test
        public void a_concurrent_scenario_failing_on_When_without_ContinueWhenFailure() {

            var scenario = Scenario.New(scenarioBehavior);
            var exception = assertThrows(Throwable.class, () -> {
                scenario
                        .Given("a correct Given without [continue when failure] setting (i.e., the default mode is [terminate when failure]", env -> {
                            assertEquals("The Given succeeded", "The Given succeeded");
                        })
                        .When("I unsuccessfully execute the scenario concurrently ", env -> {
                            assertEquals("The When succeeded", "The When failed");
                        })
                        .Then("the Then is not executed", env -> {
                            fail("Unreachable path");
                        })
                        .And("the first And is not executed", env -> {
                            sleep(100);
                            fail("Unreachable path");
                        })
                        .And("the second And is not executed", env -> {
                            sleep(50);
                            fail("Unreachable path");
                        }).ExecuteConcurrently();
            });

            assertTrue(scenario.steps().get(0).getResult().isSuccess());
            assertTrue(scenario.steps().get(1).getResult().isFailure());
            assertTrue(scenario.steps().get(2).getResult().isSkipped());
            assertTrue(scenario.steps().get(3).getResult().isSkipped());
            assertTrue(scenario.steps().get(4).getResult().isSkipped());

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter( x-> !(x.trim().isEmpty())).toList();
            assertEquals(1, errors.size());
        }

        @Test
        public void a_concurrent_scenario_executes_all_steps() {

            StringBuilder sb = new StringBuilder();
            var scenario = Scenario.New(scenarioBehavior);
            scenario
                    .Given("a correct Given staring a new concurrent group", env -> {
                        assertEquals("The Given succeeded", "The Given succeeded");
                    })
                    .When("I correctly execute the When which starts a new concurrent group", env -> {
                        assertEquals("The When succeeded", "The When succeeded");
                    })
                    .Then("the Then is executed by starting a new concurrent group", env -> {
                        sleep(400);
                        if (sb.toString().equals("The first And succeeded")){
                            sb.setLength(0);
                            sb.append("The Then succeeded");
                        }
                        assertEquals("The Then succeeded", sb.toString());
                    })
                    .And("the first And is executed concurrently along with the previous Then", env ->{
                        sleep(200);
                        if (sb.toString().equals("The second And succeeded")){
                            sb.setLength(0);
                            sb.append("The first And succeeded");
                        }
                        assertEquals("The first And succeeded", sb.toString());
                    })
                    .And("the second And is executed concurrently along with the previous Then", env ->{
                        sleep(50);
                        if (sb.isEmpty())
                            sb.append("The second And succeeded");
                        assertEquals("The second And succeeded", sb.toString());
                    }).ExecuteConcurrently();

            assertTrue(scenario.steps().get(0).getResult().isSuccess());
            assertTrue(scenario.steps().get(1).getResult().isSuccess());
            assertTrue(scenario.steps().get(2).getResult().isSuccess());
            assertTrue(scenario.steps().get(3).getResult().isSuccess());
            assertTrue(scenario.steps().get(4).getResult().isSuccess());

        }

        @Test
        public void a_concurrent_scenario_with_timeout() {

            var scenario = Scenario.New(scenarioBehavior);
            var exception = assertThrows(Throwable.class, () -> {
                scenario
                        .Given("a correct Given staring a new concurrent group", env -> {
                            assertEquals("The Given succeeded", "The Given succeeded");
                        })
                        .When("I correctly execute the When which starts a new concurrent group", env -> {
                            assertEquals("The When succeeded", "The When succeeded");
                        })
                        .Then("the Then is executed by starting a new concurrent group but it is timeout", env -> {
                            await().timeout(1, TimeUnit.SECONDS).untilAsserted(() -> {
                                sleep(10 * 1000);
                                assertTrue(true);
                            });
                        })
                        .And("the first And is executed concurrently along with the previous Then", env ->{
                            assertEquals("The first And succeeded", "The first And succeeded");
                        })
                        .And("the second And is executed concurrently along with the previous Then", env ->{
                            sleep(50);
                            assertEquals("The second And succeeded", "The second And succeeded");
                        }).ExecuteConcurrently();
            });
            assertTrue(scenario.steps().get(0).getResult().isSuccess());
            assertTrue(scenario.steps().get(1).getResult().isSuccess());
            assertTrue(scenario.steps().get(2).getResult().isFailure());
            assertTrue(scenario.steps().get(3).getResult().isSuccess());
            assertTrue(scenario.steps().get(4).getResult().isSuccess());

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter( x-> !(x.trim().isEmpty())).toList();
            assertEquals(1, errors.size());
        }

        @Test
        public void emergency_braking_and_warning_over_normal_requests() {

            Scenario concurrentScenario = Scenario.New(scenarioBehavior);
            var exception = assertThrows(Throwable.class, () -> {
                concurrentScenario
                        .Given("an outstanding request for lift to visit a floor", env -> {
                        })
                        .When("an emergency has been detected", env -> {
                        })
                        .Then("lift is stopped at nearest floor in direction of travel", env -> {
                            sleep(50);
                            assertTrue(true);
                        })
                        .And("emergency indicator should be turned on", ContinuousAfterFailure, env -> {
                            fail("Emergency indicator was not turned on");
                        })
                        .And("request should be canceled", ContinuousAfterFailure, env -> {
                            fail("Request was not canceled");
                        })
                        .Then("Lift door should be open within 5 seconds", env -> assertTrue(true))
                        .ExecuteConcurrently();
            });

            assertTrue(concurrentScenario.steps().get(0).getResult().isSuccess());
            assertTrue(concurrentScenario.steps().get(1).getResult().isSuccess());
            assertTrue(concurrentScenario.steps().get(2).getResult().isSuccess());
            assertTrue(concurrentScenario.steps().get(3).getResult().isFailure());
            assertTrue(concurrentScenario.steps().get(4).getResult().isFailure());
            assertTrue(concurrentScenario.steps().get(5).getResult().isSuccess());

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter( x-> !(x.trim().isEmpty())).toList();
            assertEquals(2, errors.size());

        }

        @Test
        public void a_fail_fast_step_failing_should_not_affect_other_steps_in_same_concurrent_group() {

            Scenario scenario = Scenario.New(scenarioBehavior);
            var exception = assertThrows(Throwable.class, () -> {
                scenario
                        .Given("an outstanding request for lift to visit a floor", env -> {
                        })
                        .When("an emergency has been detected", env -> {
                        })
                        .Then("lift is stopped at nearest floor in direction of travel", env -> {
                            sleep(50);
                            assertTrue(true);
                        })
                        .And("emergency indicator should be turned on", env -> {
                            fail("Emergency indicator was not turned on");
                        })
                        .And("request should be canceled", ContinuousAfterFailure, env -> {
                            fail("Request was not canceled");
                        })
                        .Then("Lift door should be open within 5 seconds", env -> assertTrue(true))
                        .ExecuteConcurrently();
            });

            assertTrue(scenario.steps().get(0).getResult().isSuccess());
            assertTrue(scenario.steps().get(1).getResult().isSuccess());
            assertTrue(scenario.steps().get(2).getResult().isSuccess());
            assertTrue(scenario.steps().get(3).getResult().isFailure());
            assertTrue(scenario.steps().get(4).getResult().isFailure());
            assertTrue(scenario.steps().get(5).getResult().isSkipped());

            List<String> errors = Arrays.stream(exception.getMessage().split("\n")).filter( x-> !(x.trim().isEmpty())).toList();
            assertEquals(2, errors.size());
        }

        @Test
        public void a_concurrent_scenario_starting_without_at_lease_one_Given_When_or_Then_raises_a_runtime_exception() {

            var scenario = Scenario.New(scenarioBehavior);
            var exception = assertThrows(Throwable.class, () -> {
                scenario
                    .And("emergency indicator should be turned on", ContinuousAfterFailure, env -> {
                        fail("Emergency indicator was ont turned on");
                    })
                    .And("request should be canceled", ContinuousAfterFailure, env -> {
                        fail("Request was not canceled");
                    })
                    .Then("Lift door should be open within 5 seconds", env -> assertTrue(true))
                    .ExecuteConcurrently();
            });

            String [] actual = exception.getMessage().split("\n");
            assertEquals(1, actual.length);
            assertEquals("A concurrent scenario must start with a Given, When, or Then", actual[0]);

            assertTrue(scenario.steps().get(0).getResult().isPending());
            assertTrue(scenario.steps().get(1).getResult().isPending());
            assertTrue(scenario.steps().get(2).getResult().isPending());
        }

    }

    @Nested
    @Order(100)
    class UsageExample{
        static Story usageExample;

        @BeforeAll
        public static void beforeAll() {
            usageExample = feature.newStory("Show how to use Scenario in BDD", 3)
                    .description("""
                In order to demonstrate the purpose of Scenario in BDD 
                As an ezBehave developer
                I want show an example""");
        }

        @EzScenario
        public void all_available_steps_without_passing_argument_for_lambda_in_a_scenario() {

            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(usageExample)
                    .Given("a scenario", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("a And step", env ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .But("no examples", env ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .When("I run the scenario", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenSuccess(env ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenSuccess("can have description", env ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .Then("it passes", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("I can write more assertions", env ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .But("the scenario runs only once", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenFailure(env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenFailure("can have description", env -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("each step in a scenario is executed only once", env ->{
                        stack.push(counter.getAndIncrement());
                    }).Execute();

            for (int i = stack.size()-1; i >=0 ; i--){
                assertEquals(i, stack.pop());
            }

            String expectedFullText = """
                Scenario: all available steps without passing argument for lambda in a scenario
                Given a scenario
                And a And step
                But no examples
                When I run the scenario
                Then success
                Then success can have description
                Then it passes
                And I can write more assertions
                But the scenario runs only once
                Then failure
                Then failure can have description
                And each step in a scenario is executed only once
                """;
            assertEquals(expectedFullText, usageExample.lastScenario().toString());
        }

        @EzScenario
        public void all_available_steps_passing_a_empty_Table_argument_for_lambda_in_a_scenario() {

            final AtomicInteger counter = new AtomicInteger(0);
            final Stack<Integer> stack = new Stack<>();

            Scenario.New(usageExample)
                    .Given("a scenario", table -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("a And step", table ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .But("no examples", table ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .When("I run the scenario", table -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenSuccess(env ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenSuccess("can have description", table ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .Then("it passes", table -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("I can write more assertions", table ->{
                        stack.push(counter.getAndIncrement());
                    })
                    .But("the scenario runs only once", table -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenFailure(table -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .ThenFailure("can have description", table -> {
                        stack.push(counter.getAndIncrement());
                    })
                    .And("each step in a scenario is executed only once", table ->{
                        stack.push(counter.getAndIncrement());
                    }).Execute();

            for (int i = stack.size()-1; i >=0 ; i--){
                assertEquals(i, stack.pop());
            }

            String expectedFullText = """
                Scenario: all available steps passing a empty Table argument for lambda in a scenario
                Given a scenario
                And a And step
                But no examples
                When I run the scenario
                Then success
                Then success can have description
                Then it passes
                And I can write more assertions
                But the scenario runs only once
                Then failure
                Then failure can have description
                And each step in a scenario is executed only once
                """;
            assertEquals(expectedFullText, usageExample.lastScenario().toString());
        }
    }

    private static void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
