package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import tw.teddysoft.ezspec.exception.PendingException;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

@EzFeature
public class FeatureWithDefaultStorySpec {
    static Feature feature;

    static String name;
    static String description;

    @BeforeAll
    public static void beforeAll() {
        name = "Create an ezSpec feature";
        description = "FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow";
        feature = Feature.New(name, description);
    }

    @Test
    public void new_scenario_with_default_story() {
        var defaultStory = feature.newDefaultStory();
        feature.newScenario();

        assertEquals("new_scenario_with_default_story", defaultStory.lastScenario().getName());
        assertEquals("""
                Feature: Create an ezSpec feature
                                
                FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow
                                
                Scenario: new scenario with default story
                                
                
                """, feature.toString());

    }

    @Test
    public void new_scenario_outline_with_default_story() {
        feature.newDefaultStory();
        var examples = """
                | used      | debt        | score     |   result  |
                | 40%       | $202,704    | 499       |   reject  |
                | 41%       | $202,704    | 500       |   accept  |
                """;
        feature.newScenarioOutline()
                .WithExamples(examples);

        assertEquals("""
                Feature: Create an ezSpec feature
                                
                FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow
                                
                Scenario outline: new scenario outline with default story
                                
                                
                Examples:\s
                |	used	|	debt	|	score	|	result	|
                |  40%  	|$202,704	|   499   	|  reject  	|
                |  41%  	|$202,704	|   500   	|  accept  	|
                                
                                
                """, feature.toString());
    }

    @Test
    public void new_second_default_story_will_replace_previous() {
        var story1 = feature.newDefaultStory();
        var story2 = feature.newDefaultStory();

        assertNotSame(story1, feature.getDefaultStory());
        assertSame(story2, feature.getDefaultStory());
    }

    @Test
    public void new_default_story_fail_when_other_story_exist() {
        Feature localFeature = Feature.New("local feature");
        localFeature.newStory("story");

        assertThrows(RuntimeException.class, () -> localFeature.newDefaultStory());
    }

}
