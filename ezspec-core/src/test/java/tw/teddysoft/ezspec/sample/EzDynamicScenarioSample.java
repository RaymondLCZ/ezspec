package tw.teddysoft.ezspec.sample;

import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import tw.teddysoft.ezspec.EzFeature;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.exception.PendingException;
import tw.teddysoft.ezspec.extension.junit5.EzDynamicScenario;
import tw.teddysoft.ezspec.extension.junit5.EzDynamicScenarioOutline;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.ezspec.scenario.Step.ContinuousAfterFailure;

@EzFeature
public class EzDynamicScenarioSample {

    @EzDynamicScenario
    DynamicNode test_EzDynamicScenario_annotation_all_pass() {
        return Feature.New("Dynamic scenario outline").newStory("default")
                .newScenario()
                .Given("a successful given", env -> {
                    assertEquals("a", "a");
                })
                .When("I execute a successful when", env -> {
                    assertEquals("c", "c");
                })
                .Then("a successful then is executed", env -> {
                    assertTrue(true);
                })
                .DynamicExecute();
    }

    @EzDynamicScenario
    DynamicNode test_EzDynamicScenario_annotation_with_then_pending() {
        return Feature.New("Dynamic scenario outline").newStory("default")
                .newScenario()
                .Given("a successful given", env -> {
                    assertEquals("a", "a");
                })
                .When("I execute a successful when", env -> {
                    assertEquals("c", "c");
                })
                .Then("a pending then is executed", env -> {
                    PendingException.pending("Not implemented.");
                })
                .DynamicExecute();
    }

    @EnabledIfEnvironmentVariable(named = "SAMPLE_TESTS", matches = "true")
    @EzDynamicScenario
    DynamicNode test_DynamicScenario_annotation_without_ContinuousAfterFailure() {

        return Feature.New("Dynamic scenario outline").newStory("default")
                .newScenario()
                .Given("a successful given", env -> {
                    assertEquals("a", "a");
                })
                .When("I execute a failed when", env -> {
                    assertEquals("c", "d");
                })
                .Then("a then is skipped", env -> {
                    assertTrue(true);
                })
                .DynamicExecute();
    }

    @EnabledIfEnvironmentVariable(named = "SAMPLE_TESTS", matches = "true")
    @EzDynamicScenario
    DynamicNode test_DynamicScenario_annotation_with_ContinuousAfterFailure() {

        return Feature.New("Dynamic scenario outline").newStory("default")
        .newScenario()
            .Given("a failed given", ContinuousAfterFailure, env -> {
                fail("Given failed and continuously execute");
            })
            .When("I execute a failed when", ContinuousAfterFailure, env -> {
                fail("When failed and continuously execute");
            })
            .Then("a failed then is executed", ContinuousAfterFailure, env -> {
                fail("Then failed and continuously execute");
            })
            .DynamicExecute();
    }

    @EnabledIfEnvironmentVariable(named = "SAMPLE_TESTS", matches = "true")
    @EzDynamicScenario
    DynamicNode no_continue_when_failure_scenario_with_Given_And_When_Then() {

        return Feature.New("Dynamic scenario outline").newStory("default")
                .newScenario()
                .Given("a failed given", env -> {
                    fail("Given failed and stop execution");
                })
                .When("should not be execute", env -> {
                    fail("When should not be executed");
                })
                .Then("should not be execute", env -> {
                    fail("Then should not be execute");
                })
                .DynamicExecute();
    }

    @EzDynamicScenario
    DynamicNode all_available_steps_without_passing_argument_for_lambda_in_a_scenario() {

        final AtomicInteger counter = new AtomicInteger(0);
        final Stack<Integer> stack = new Stack<>();

        return Feature.New("Dynamic scenario outline").newStory("default")
                .newScenario()
                .Given("a scenario", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .And("a And step", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .But("no examples", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .When("I run the scenario", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .ThenSuccess(env -> {
                    stack.push(counter.getAndIncrement());
                })
                .ThenSuccess("can have description", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .Then("it passes", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .And("I can write more assertions", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .But("the scenario runs only once", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .ThenFailure(env -> {
                    stack.push(counter.getAndIncrement());
                })
                .ThenFailure("can have description", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .And("each step in a scenario is executed only once", env -> {
                    stack.push(counter.getAndIncrement());
                })
                .DynamicExecute();
    }

    @EnabledIfEnvironmentVariable(named = "SAMPLE_TESTS", matches = "true")
    @EzDynamicScenarioOutline
    DynamicNode scenario_outline_with_description() {
        final String examples = """
                | used      | debt        | score     |   result  |
                | 40%       | $202,704    | 499       |   reject  |
                | 41%       | $202,704    | 500       |   accept  |
                """;

        return Feature.New("My feature").newStory("A scenario outline with description", 1)
                .description("""
                    In order to explain a scenario outline
                    As a bdd participant
                    I want to specify description in a scenario outline
                    """)
                .newScenarioOutline("Screening tenant leads based on credit score", """
                        TENANTS PIPELINE is a list of verified tenant leads a landlord can choose from.
                        
                        Credit score is calculated by an external auditor
                        through their API and has a range of 300-850.
                        """)
                .WithExamples(examples)
                .Given("a tenant lead Simona Jenkins with credit used <used> and total debt <debt>:", ContinuousAfterFailure, env -> {
                    assertEquals("40%", env.gets("used"));
                })
                .And("that the lead waits in the queue to the tenants pipeline", env -> {
                })
                .When("the tenant lead has a <score>", env -> {
                })
                .Then("we should <result> the lead", env -> {
                }).DynamicExecute();
        }
}
