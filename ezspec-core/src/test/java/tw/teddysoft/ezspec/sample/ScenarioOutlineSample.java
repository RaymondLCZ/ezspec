package tw.teddysoft.ezspec.sample;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import tw.teddysoft.ezspec.Feature;
import tw.teddysoft.ezspec.Story;
import tw.teddysoft.ezspec.example.Example;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScenarioOutlineSample {

    static Feature feature;
    static Story storyHavingScenarioOutlines;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("ScenarioOutline is a Scenario with Examples");
        storyHavingScenarioOutlines = feature.newStory("Scenario with examples")
                .description("""
                    In order to verify a scenario with different data sets
                    As a bdd participant
                    I want to specify examples in a scenario""");
    }

    @EnabledIfEnvironmentVariable(named = "SAMPLE_TESTS", matches = "true")
    @EzScenarioOutline
    public void execute_concurrently() {

        final String tableLettersAndNumbers = """
                | Password  | Length            | Valid or Invalid  |
                | abc1      | 4                 | valid             |
                """;
        Example lettersAndNumbers = new Example("Letters and Numbers",
                "Passwords need both letters and numbers to be valid",
                tableLettersAndNumbers);

        feature.withStory(storyHavingScenarioOutlines.getName()).
                newScenarioOutline()
                .WithExamples(lettersAndNumbers)
                .Given("I try to create an account with password <Password>", true, env -> {
                    assertEquals(env.getInput().get("Password"), env.gets("Password"));
                })
                .Then("the password is <Length>", env -> {
                    assertEquals("3", env.gets("Length"));
                })
                .And("I should see that the password is <Valid or Invalid>", env -> {
                    assertEquals("valid", env.gets("Valid or Invalid"));
                }).ExecuteConcurrently();
    }

}
