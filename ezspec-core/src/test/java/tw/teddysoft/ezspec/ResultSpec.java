package tw.teddysoft.ezspec;

import org.junit.jupiter.api.Test;
import tw.teddysoft.ezspec.scenario.Result;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EzFeature
public class ResultSpec {

    @Test
    public void is_success(){
        Result result = Result.Success();
        assertTrue(result.isSuccess());
        assertFalse(result.isFailure());
        assertFalse(result.isPending());
        assertFalse(result.isSkipped());
    }

    @Test
    public void is_failure(){
        Result result = Result.Failure(new RuntimeException());
        assertTrue(result.isFailure());
        assertFalse(result.isSuccess());
        assertFalse(result.isPending());
        assertFalse(result.isSkipped());
    }

    @Test
    public void is_skipped(){
        Result result = Result.Skip();
        assertTrue(result.isSkipped());
        assertFalse(result.isSuccess());
        assertFalse(result.isFailure());
        assertFalse(result.isPending());
    }

    @Test
    public void is_pending(){
        Result result = Result.Pending(new RuntimeException());
        assertTrue(result.isPending());
        assertFalse(result.isSuccess());
        assertFalse(result.isFailure());
        assertFalse(result.isSkipped());
    }
}
