package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DynamicNode;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.extension.junit5.EzDynamicScenario;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static tw.teddysoft.ezspec.scenario.Step.ContinuousAfterFailure;

@EzFeature
public class EzSpecUsageTest {
    static Feature feature;
    static final int StoryForScenario = 1;
    static final int StoryForScenarioOutline = 2;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("ezSpec usage feature");
        feature.newStory("A story for using @EzScenario", StoryForScenario);
        feature.newStory("A story for using @EzScenarioOutline", StoryForScenarioOutline);
    }

    @EzScenario
    public void scenario_using_EzScenario_annotation() {
        feature.withStory(StoryForScenario).
                newScenario("scenario")
                .Given(" a Given", env -> {
                    assertTrue(true);
                })
                .And("a And", env -> {
                    assertTrue(true);
                })
                .When("I execute a When", env -> {
                    assertTrue(true);
                })
                .Then("a Then should be executed", env -> {
                    assertTrue(true);
                }).Execute();
    }

    @EzDynamicScenario
    DynamicNode scenario_outline_with_description() {
        final String examples = """
                | used      | debt        | score     |   result  |
                | 40%       | $202,704    | 499       |   reject  |
                | 41%       | $202,704    | 500       |   accept  |
                """;

        return feature.newStory("A scenario outline with description", 1)
                .description("""
                    In order to explain a scenario outline
                    As a bdd participant
                    I want to specify description in a scenario outline
                    """).newScenarioOutline("Screening tenant leads based on credit score", """
                        TENANTS PIPELINE is a list of verified tenant leads a landlord can choose from.
                        
                        Credit score is calculated by an external auditor
                        through their API and has a range of 300-850.
                        """)
                .WithExamples(examples)
                .Given("a tenant lead Simona Jenkins with credit used <used> and total debt <debt>:", ContinuousAfterFailure, env -> {
                    assertEquals(env.getInput().get("used"), env.gets("used"));
                })
                .And("that the lead waits in the queue to the tenants pipeline", env -> {
                })
                .When("the tenant lead has a <score>", env -> {
                })
                .Then("we should <result> the lead", env -> {
                }).DynamicExecute();
    }

    public static String deleteErrorLine(String output){
        String[] outputs = output.split("\n");
        StringBuilder sb = new StringBuilder();
        for (var s : outputs) {
            if (!s.trim().startsWith("at ") && !s.trim().isEmpty()) {
                sb.append(s).append("\n");
            }
        }
        return sb.toString();
    }

    private void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
