package tw.teddysoft.ezspec;

import org.junit.jupiter.api.*;
import org.junit.platform.suite.api.IncludeTags;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;
import tw.teddysoft.ezspec.extension.junit5.EzScenarioOutline;
import tw.teddysoft.ezspec.scenario.Background;
import tw.teddysoft.ezspec.scenario.Scenario;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@EzFeature
public class BackgroundSpec {
    static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("Background");
    }

    @Nested
    @Order(1)
    @IncludeTags(EzSpecTag.CQRS.Command)
    class ShareStepAndData {
        static Story useBackgroundStory;

        UUID userId;
        int backgroundSideEffect = 0;

        @BeforeAll
        public static void beforeAll() {
            useBackgroundStory = feature.newStory("Extract common steps in Background for sharing")
                    .description("""
                        In order to eliminate duplication and shared context (i.e., test fixture) among all scenarios in a story
                        As a bdd participant
                        I want to specify a Background
                        """);
        }

        @BeforeEach
        public void create_a_userId_in_background() {
            feature.withStory(useBackgroundStory.getName()).newBackground()
                    .Given("I execute a shared step", env -> {
                        backgroundSideEffect = 100;
                        env.put("name", "Teddy");
                    })
                    .And("I crate a userId of type UUID in Background and share it via 'MY_UUID' key", env -> {
                        userId = UUID.randomUUID();
                        env.put("MY_UUID", userId);
                    }).Execute();

            String expectedFullText = """
                Background: create a userId in background
                Given I execute a shared step
                And I crate a userId of type UUID in Background and share it via 'MY_UUID' key""";
            assertEquals(expectedFullText, useBackgroundStory.getBackground().toString());
        }

        @EzScenario
        public void first_scenario() {
            feature.withStory(useBackgroundStory.getName()).newScenario()
                    .When("I execute a scenario", env -> {})
                    .Then("I am effected by the side effect caused by the step in Background", env ->{
                        assertEquals("Teddy", env.gets("name"));
                    })
                    .And("I can read the userId created in the Background", env -> {
                        assertEquals(userId, (env.get("MY_UUID", UUID.class)));
                    }).Execute();

            String expectedFullText = """
                Scenario: first scenario
                When I execute a scenario
                Then I am effected by the side effect caused by the step in Background
                And I can read the userId created in the Background
                """;
            assertEquals(expectedFullText, useBackgroundStory.lastScenario().toString());

        }

        @EzScenario
        public void second_scenario() {
            feature.withStory(useBackgroundStory.getName()).newScenario()
                    .When("I execute another scenario", env -> {})
                    .Then("I am effected by the side effect caused by the step in Background", env ->{
                        assertEquals("Teddy", env.gets("name"));
                    })
                    .And("I can read the userId created in the Background", env -> {
                        assertEquals(userId, (env.get("MY_UUID", UUID.class)));
                    }).Execute();
        }

        @EzScenarioOutline
        public void Invite_team_members_of_different_team_roles() {
            String examples = """
                | invited_user_id | team_role |
                | Ada             | Admin     |
                | Eiffel          | Staff     |
                """;
            feature.withStory(useBackgroundStory.getName()).newScenarioOutline()
                    .WithExamples(examples)
                    .When("he invites <invited_user_id> to his team as a team <team_role>", env -> {
                        assertEquals("Teddy", env.gets("name"));
                    })
                    .Then("<invited_user_id> becomes a team <team_role> of Teddy's team", env -> {
                        assertEquals(userId, (env.get("MY_UUID", UUID.class)));
                    })
                    .Execute();
        }

    }

    @Nested
    @Order(10)
    class BackgroundAnnotatedWithBeforeEach {
        static Story backgroundAnnotatedWithBeforeEach;

        @BeforeAll
        public static void beforeAll() {
            backgroundAnnotatedWithBeforeEach = feature.newStory("Background annotated with @BeforeEach", 1)
                    .description("""
                        In order to execute Background
                        As an ezBehave user
                        I need to put Background in a method annotated with @BeforeEach
                        """);
        }


        @BeforeEach
        public void background_must_in_a_method_wim_BeforeEach_annotation() {
            Background.New(backgroundAnnotatedWithBeforeEach)
                    .Given("the first Background in a method annotated with @BeforeEach", env -> {})
                    .And("the second Background put in a method without @BeforeEach annotation", env ->{})
                    .And("I create a UUID in the first Background and shared it", env ->{
                        env.put("with_BeforeEach", UUID.randomUUID());
                    })
                    .And("I also create a UUID in the second Background and shared it", env ->{})
                    .Execute();
        }

        public void background_without_BeforeEach_annotation() {
            Background.New(backgroundAnnotatedWithBeforeEach)
                    .Given("I crate a userId of type UUID in Background and share it via 'MY_UUID' key to be used in scenarios later", env -> {
                        env.put("without_BeforeEach", UUID.randomUUID());
                    }).Execute();
        }

        @EzScenario
        public void try_to_read_data_created_in_both_background() {
            Scenario.New(backgroundAnnotatedWithBeforeEach)
                    .When("I execute a scenario", env -> {})
                    .Then("I can read data created in the first Background with @BeforeEach", env ->{
                        assertNotNull(env.get("with_BeforeEach", UUID.class));
                    })
                    .But("I cannot read data create in the second Background without @BeforeEach", env -> {
                        assertTrue(env.gets("without_BeforeEach").isEmpty());
                    }).Execute();
        }
    }

    @Nested
    @Order(20)
    class StoryCanHaveOnlyOneBackground {
        static Story overwriteFirstBackground = feature.newStory("Latter specified Background overrides its precedent", 2)
                .description("""
                        In order to simplify shared context within a story
                        As an ezBehave developer
                        I only allow a story to support one Background
                        And if multiple Backgrounds are specified,
                        the latter one overrides its precedent
                        """);

        @BeforeEach
        public void first_background() {
            Background.New(overwriteFirstBackground)
                    .Given("I share 'user = Teddy' in the first Background", env -> {
                        env.put("user", "Teddy");
                    }).And("I share 'name = Eiffel' in the second Background", env -> {})
                    .Execute();
        }

        @BeforeEach
        public void second_background() {
            Background.New(overwriteFirstBackground)
                    .Given("I share 'user = Teddy' in the first Background", env -> {
                    })
                    .And("I share 'name = Eiffel' in the second Background", env -> {
                        env.put("cat", "Eiffel");
                    })
                    .Execute();
        }

        @EzScenario
        public void read_data_created_in_both_background() {
            Scenario.New(overwriteFirstBackground)
                    .When("I execute a scenario", env -> {})
                    .Then("I can only read data created in the second Background", env ->{
                        assertEquals("Eiffel", env.gets("cat"));
                    })
                    .But("cannot read data created in the first Background because it was overridden by the second one", env ->{
                        assertTrue(env.gets("user").isEmpty());
                    })
                    .Execute();

            String expectedFullText = """
                    Background: second background
                    Given I share 'user = Teddy' in the first Background
                    And I share 'name = Eiffel' in the second Background""";
            assertEquals(expectedFullText, overwriteFirstBackground.getBackground().toString());
        }
    }


    @Nested
    @Order(100)
    class UsageExample{
        static Story usageExample;

        @BeforeAll
        public static void beforeAll() {
            usageExample = feature.newStory("Usage Example", 3)
                    .description("""
                In order to demonstrate the purpose of Background in BDD
                As an ezBehave developer
                I want to show an example taken from BDD in Action
                """);
        }

        @BeforeEach
        public void Martin_is_registered_on_the_site() {
            Background.New(usageExample)
                    .Given("Martin is a Frequent Flyer member", env -> {})
                    .And("Martin has registered online with a password of 'secret'", env -> {
                        assertTrue(true);
                    })
                    .Execute();
        }

        @EzScenario
        public void logging_on_successfully() {
            Scenario.New(usageExample)
                    .When("Martin logs on with password 'secret'", env -> {})
                    .Then("he should be given access to the site", env -> {
                        assertTrue(true);
                    })
                    .Execute();
        }

        @EzScenario
        public void logging_on_with_an_incorrect_password() {
            Scenario.New(usageExample)
                    .When("Martin logs on with password 'wrong'", env -> {
                        assertTrue(true);
                    })
                    .Then("he should be informed that his password was incorrect", env -> {
                        assertTrue(true);
                    })
                    .Execute();
        }
    }

    @Nested
    @Order(200)
    class DefineBackgroundNameExample{
        static Story usageExample;

        @BeforeAll
        public static void beforeAll() {
            usageExample = feature.newStory("Usage Example", 3)
                    .description("""
                In order to demonstrate the purpose of Background in BDD
                As an ezBehave developer
                I want to show an example taken from BDD in Action
                """);
        }

        @BeforeEach
        public void setUp() {
            Background.New("Martin is registered on the site", usageExample)
                    .Given("Martin is a Frequent Flyer member", env -> {})
                    .And("Martin has registered online with a password of 'secret'", env -> {
                        assertTrue(true);
                    })
                    .Execute();
        }

        @EzScenario
        public void logging_on_successfully() {
            Scenario.New(usageExample)
                    .When("Martin logs on with password 'secret'", env -> {})
                    .Then("he should be given access to the site", env -> {
                        assertTrue(true);
                    })
                    .Execute();
        }
    }
}
