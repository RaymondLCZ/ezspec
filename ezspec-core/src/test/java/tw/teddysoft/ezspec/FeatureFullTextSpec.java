package tw.teddysoft.ezspec;

import org.junit.jupiter.api.*;
import tw.teddysoft.ezspec.exception.PendingException;
import tw.teddysoft.ezspec.extension.junit5.EzScenario;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EzFeature
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FeatureFullTextSpec {
    static Feature feature;

    static String name;
    static String description;

    @BeforeAll
    public static void beforeAll() {
        name = "Create an ezSpec feature";
        description = "FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow";
        feature = Feature.New(name, description);
    }

    @Test
    @Order(1)
    public void full_text() {
        feature.newStory("Prepare to document requirements in a feature");

        var expected = """
                Feature: Create an ezSpec feature
                                
                FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow
                                
                Story: Prepare to document requirements in a feature
                                
                """;
        assertEquals(expected, feature.toString());
    }

    @EzScenario
    @Order(2)
    public void prepare_a_feature_file() {
        feature.newStory("Prepare to document requirements in a feature")
                .description("""
                        In order to describe a function unit that delivers values for users
                        As a developer
                        I want to write a feature
                        """.trim())
                .newScenario()
                .Given("users are ready to discuss requirements with me", env -> {
                })
                .When("I declare a static feature object in a JUnit test class", env -> {
                })
                .And(format("I instantiate it with ${name=%s} and ${description=%s} in a static method annotated with @BeforeAll", name, description), env -> {
                })
                .Then("a feature file should be ready to use (i.e., to write a story)", env -> {
                    assertTrue(feature != null);
                    assertEquals(feature.getName(), env.getHistoricalArg("name"));
                    assertEquals(feature.getDescription(), env.getHistoricalArg("description"));
                })
                .Execute();
    }

    @EzScenario
    @Order(3)
    public void prepare_a_feature_file2() {
        feature.newStory("Prepare to document requirements in a feature")
                .description("""
                        In order to describe a function unit that delivers values for users
                        As a developer
                        I want to write a feature
                        """.trim())
                .newScenario()
                .Given("users are ready to discuss requirements with me", env -> {
                    throw new PendingException();
                })
                .When("I declare a static feature object in a JUnit test class", env -> {
                })
                .And(format("I instantiate it with ${name=%s} and ${description=%s} in a static method annotated with @BeforeAll", name, description), env -> {
                })
                .Then("a feature file should be ready to use (i.e., to write a story)", env -> {
                })
                .Execute();
    }

    @Test
    @Order(4)
    public void last_feature_full_text() {
        feature.newStory("Prepare to document requirements in a feature");

        var expected = """
                Feature: Create an ezSpec feature
                                
                FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow
                                
                Story: Prepare to document requirements in a feature
                                
                Story: Prepare to document requirements in a feature
                In order to describe a function unit that delivers values for users
                As a developer
                I want to write a feature
                Scenario: prepare a feature file
                Given users are ready to discuss requirements with me
                When I declare a static feature object in a JUnit test class
                And I instantiate it with Create an ezSpec feature and FEATURE in a static method annotated with @BeforeAll
                Then a feature file should be ready to use (i.e., to write a story)
                                
                                
                Story: Prepare to document requirements in a feature
                In order to describe a function unit that delivers values for users
                As a developer
                I want to write a feature
                Scenario: prepare a feature file2
                Given users are ready to discuss requirements with me
                When I declare a static feature object in a JUnit test class
                And I instantiate it with Create an ezSpec feature and FEATURE in a static method annotated with @BeforeAll
                Then a feature file should be ready to use (i.e., to write a story)
                                
                                
                Story: Prepare to document requirements in a feature
                
                """;
        assertEquals(expected, feature.toString());
    }

}
