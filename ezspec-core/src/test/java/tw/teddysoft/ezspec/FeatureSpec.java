package tw.teddysoft.ezspec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@EzFeature
public class FeatureSpec {
    static Feature feature;

    static String name;
    static String description;

    @BeforeAll
    public static void beforeAll() {
        name = "Create an ezSpec feature";
        description = "FEATURE: In ezSpec a feature is an object similar to a feature file in Cucumber or SpecFlow";
        feature = Feature.New(name, description);
        feature.newDefaultStory();
    }

    @Test
    public void create_a_new_story_fails_when_default_story_exist() {
        var err = assertThrows(RuntimeException.class, () -> feature.newStory("new story"));
        assertEquals("require default story not exist", err.getMessage());
    }

    @Test
    public void create_a_new_story_with_order_fails_when_default_story_exist() {
        var err = assertThrows(RuntimeException.class, () -> feature.newStory("new story", 0));
        assertEquals("require default story not exist", err.getMessage());
    }
}
