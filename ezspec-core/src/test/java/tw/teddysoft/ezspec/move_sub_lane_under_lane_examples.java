package tw.teddysoft.ezspec;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import tw.teddysoft.ezspec.extension.junit5.Junit5Examples;
import tw.teddysoft.ezspec.table.Table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static tw.teddysoft.ezspec.extension.junit5.JunitUtils.constructTable;

public class move_sub_lane_under_lane_examples extends Junit5Examples {

    public static final List<String> HEADER = new ArrayList<>(Arrays.asList("example_code", "lane_name", "new_parent_name", "new_position", "event_count", "given_workflow", "expected_workflow"));
    public static final String WORKFLOW = "Workflow";


    @Override
    public String getDescription() {
        return "A sub lane can be moved under another lane.";
    }

    @Override
    public Table getTable() {
        return constructTable(HEADER, provideArguments(null));
    }

    @Override
    protected String getExamplesRawData() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {

        //region : Example Data
        String givenWorkflow = """
                | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                | To Do         | to_do        |    -1         |   Stage      |   3        |  
                | Doing         | doing        |    -1         |   Stage      |   5        |  
                | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                | Design        | design       |    team_a     |   Stage      |   4        |
                | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                | Bug           | bug          |    doing      |   Swimlane   |   -1       |                 
                  """;

        String expectedSubStageToSubStage = """
                    | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                    | To Do         | to_do        |    -1         |   Stage      |   3        |  
                    | Doing         | doing        |    -1         |   Stage      |   5        |  
                    | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                    | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                    | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                    | Design        | design       |    team_a     |   Stage      |   4        |
                    | Step 1        | step_1       |    step_3     |   Stage      |   -1       |
                    | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                    | Bug           | bug          |    doing      |   Swimlane   |   -1       |                 
                      """;

        String expectedSubStageToSubSwimlane = """
                    | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                    | To Do         | to_do        |    -1         |   Stage      |   3        |  
                    | Doing         | doing        |    -1         |   Stage      |   5        |  
                    | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                    | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                    | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                    | Bug           | bug          |    doing      |   Swimlane   |   -1       |                 
                    | Step 2        | step_2       |    feature    |   Stage      |   -1       |
                    | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                    | Design        | design       |    team_a     |   Stage      |   4        |
                      """;

        String expectedSubStageToRootStage = """
                    | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                    | To Do         | to_do        |    -1         |   Stage      |   3        |  
                    | Doing         | doing        |    -1         |   Stage      |   5        |  
                    | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                    | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                    | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                    | Design        | design       |    to_do      |   Stage      |   4        |
                    | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                    | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                    | Bug           | bug          |    doing      |   Swimlane   |   -1       |                 
                      """;

        String expectedSubSwimlaneToSubStage = """
                    | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                    | To Do         | to_do        |    -1         |   Stage      |   3        |  
                    | Doing         | doing        |    -1         |   Stage      |   5        |  
                    | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                    | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                    | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                    | Team A        | team_a       |    step_2     |   Swimlane   |   -1       |
                    | Feature       | feature      |    step_2     |   Swimlane   |   -1       |
                    | Design        | design       |    team_a     |   Stage      |   4        |
                    | Bug           | bug          |    doing      |   Swimlane   |   -1       |                 
                      """;

        String expectedSubSwimlaneToSubSwimlane = """
                    | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |  
                    | To Do         | to_do        |    -1         |   Stage      |   3        |  
                    | Doing         | doing        |    -1         |   Stage      |   5        |  
                    | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                    | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                    | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                    | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                    | Bug           | bug          |    doing      |   Swimlane   |   -1       |                 
                    | Team A        | team_a       |    feature    |   Swimlane   |   -1       |
                    | Design        | design       |    team_a     |   Stage      |   4        |
                      """;

        String expectedSubSwimlaneToRootStage = """
                    | lane_name     | lane_id      |  parent_id    | lane_type    | wip_limit  |
                    | To Do         | to_do        |    -1         |   Stage      |   3        |
                    | Doing         | doing        |    -1         |   Stage      |   5        |
                    | Step 1        | step_1       |    to_do      |   Stage      |   -1       |
                    | Step 2        | step_2       |    to_do      |   Stage      |   -1       |
                    | Step 3        | step_3       |    to_do      |   Stage      |   -1       |
                    | Feature       | feature      |    doing      |   Swimlane   |   -1       |
                    | Team A        | team_a       |    doing      |   Swimlane   |   -1       |
                    | Bug           | bug          |    doing      |   Swimlane   |   -1       |
                    | Design        | design       |    team_a     |   Stage      |   4        |
                      """;
        //endregion

        return Stream.of(
                Arguments.of("ML-B01", "Step 1", "Step 3", "0", 1, givenWorkflow, expectedSubStageToRootStage),
                Arguments.of("ML-B02", "Step 2", "Feature", "0", 1, givenWorkflow, expectedSubSwimlaneToSubStage),
                Arguments.of("ML-B03", "Design", "To Do", "3", 1, givenWorkflow, expectedSubSwimlaneToSubSwimlane),
                Arguments.of("ML-B04", "Feature", "Step 2", "1", 1, givenWorkflow, expectedSubSwimlaneToRootStage)
        );
    }
}
