# ezSpec-core

**ezSpec-core** is a developer-facing Behavior-Driven Development (BDD) tool implemented in Java. It adopts Gherkin syntax to specify requirements in a Specification by Example (SBE) manner. The goal of ezSpec-core is to provide a BDD/SBE tool that is more developer-friendly than current tools.

Common BDD/SBE tools such as Cucumber, SpecFlow, and JBehave are external Domain-Specific Languages (DSL). Developers first write plain text feature files that are parsed by the tools to generate step definitions for particular programming languages such as Ruby, C#, and Java. Developers then implement the step definitions to write executable specifications.

Unlike common BDD/SBE tools, ezSpec-core is an internal DSL. Both feature files and step definitions are written in Java. The former is a JUnit 5 test case and the latter is a Java lambda expression. Due to the internal DSL approach taken by ezSpec-core, the context switch between feature files written and step definition generation as well as written are removed.

## How to use

### Requirement

- JDK 21+

### Maven

```xml
<dependency>
    <groupId>tw.teddysoft.ezspec</groupId>
    <artifactId>ezspec-core</artifactId>
    <version>LATEST</version>
</dependency>
```

### Documentation

- [Javadoc](https://www.javadoc.io/doc/tw.teddysoft.ezspec/ezspec-core)

### Usage

#### Feature

The `Feature` class represents the feature keyword in Gherkin which provides a high-level description of a software feature. A feature in Gherkin contains one or more scenarios. Unlike Gherkin, the `Feautre` class in ezSpec contains stories that contains scenarios. For developers who do not need the `Story` concept, the `Feature` class provides methods to add scenarios directly without specifying a story. The directly added scenarios are attached to a default story in the `Feature` class.

A `Feature` is defined as a static data member of a JUnit 5 test class and initiated in the `@BeforeAll` method.

##### @EzFeature

The `@EzFeature` annotation represents a test class that is written by ezSpec. `@EzFeature` provides following functionalities:

- `@EzFeature` contains `@Tag(EzSpecTag.LivingDoc.EzSpec)`, which is used for identifying the tests written by `EzSpec`.
- `@EzFeature` converts snake case method name by replacing underscores with white spaces.

#### Story

The `Story` class represents a user story in agile software development. A story is a planning tool for slicing a feature. ezSpec supports developers to break down a feature into smaller stories. A feature may be too big to be implemented in an iteration. A story is small enough to be completed in an iteration.

Many BDD participants suggest that the story concept is not necessary because it is a planning element rather than a specification element. Therefore, ezSpec allows developers to hide stories by directly attaching scenarios to the default story in a feature.

- Default Story:
  - To create a default story in a feature, invoke `Feature::newDefaultStory()`.
  - A default story can only be created when there's no any other story.
- Story:
  - To create a story in a feature, invoke `Feature::newStory(storyName)`.
  - If a default story exists, users cannot create a new story.

#### Scenario and ScenarioOutline

`Scenario` represents a concrete example that illustrates a business rule of a feature. It consists of a list of steps. `ScenarioOutline` is an alternative to `Scenario` that accepts multiple examples with the same steps.

- Scenario
  - To create a scenario with a default story, invoke `Feature::newScenario()`.
  
    ```java
    @EzFeature
    public class ScenarioExample {
      static Feature feature;

      @BeforeAll
      public static void beforeAll() {
          feature = Feature.New("Scenario example using default story");
          feature.newDefaultStory();
      }

      @EzScenario
      public void scenario_example_with_default_story() {
          feature.newScenario()
                  .Given("the tax excluded price of a computer is $20000", env -> {
                      assertTrue(env.hasArgument());
                      assertEquals(1, env.getArgs().size());
                      assertEquals("20000", env.getArgs().get(0).value());
                      assertEquals("20000", env.getArg(0));

                      env.put("tax excluded price", env.getArgi(0));
                  })
                  .And("the VAT rate is ${VAT=0.05}", env -> {
                      assertEquals(1, env.getArgs().size());
                      assertEquals("0.05", env.getArg("VAT"));
                      assertEquals("VAT", env.getArgs().get(0).key());

                      env.put("vat rate", env.getArgd("VAT"));
                  })
                  .When("I buy the computer", env ->{
                      double price = env.get("tax excluded price", Integer.class) * (1 + env.get("vat rate", Double.class));
                      env.put("tax included price", price);
                  })
                  .Then("I need to pay ${total_price:21,000}", env ->{
                      assertEquals(env.getArgi("total_price"), env.get("tax included price", Double.class));
                  })
                  .Execute();
      }
    }
    ```

  - To create a scenario with a story, invoke `Scenario.New(story)` or `Scenario.New(scenarioName, story)`.

    ```java
    @EzFeature
    public class ScenarioExample {
        static Feature feature;
        static Story story;

        @BeforeAll
        public static void beforeAll() {
            feature = Feature.New("Scenario example using default story");
            story = feature.newStory("Tax calculation");
        }

        @EzScenario
        public void scenario_example_with_user_created_story() {
            Scenario.New(story)
                    .Given("the tax excluded price of a computer is $20000", env -> {
                        assertTrue(env.hasArgument());
                        assertEquals(1, env.getArgs().size());
                        assertEquals("20000", env.getArgs().get(0).value());
                        assertEquals("20000", env.getArg(0));

                        env.put("tax excluded price", env.getArgi(0));
                    })
                    .And("the VAT rate is ${VAT=0.05}", env -> {
                        assertEquals(1, env.getArgs().size());
                        assertEquals("0.05", env.getArg("VAT"));
                        assertEquals("VAT", env.getArgs().get(0).key());

                        env.put("vat rate", env.getArgd("VAT"));
                    })
                    .When("I buy the computer", env ->{
                        double price = env.get("tax excluded price", Integer.class) * (1 + env.get("vat rate", Double.class));
                        env.put("tax included price", price);
                    })
                    .Then("I need to pay ${total_price:21,000}", env ->{
                        assertEquals(env.getArgi("total_price"), env.get("tax included price", Double.class));
                    })
                    .Execute();
        }
    }
    ```

  - Use `@EzScenario` instead of `@Test`.

- ScenarioOutline
  - To create a scenario outline with a default story, invoke `Feature::newScenarioOutline()`.

    ```java
    @EzFeature
    public class ScenarioOutlineExample {
        static Feature feature;

        @BeforeAll
        public static void beforeAll() {
            feature = Feature.New("scenario outline example");
            feature.newDefaultStory();
        }

        @EzScenarioOutline
        public void scenario_outline_example_with_table_input() {
            String examples = """
                    | tax_excluded | vat_rate | total_price |
                    | 20000        | 0.05     | 21000       |
                    | 10000        | 0.01     | 10100       |
                    | 35000        | 0.10     | 38500       |
                    """;

            feature.newScenarioOutline()
                    .WithExamples(examples)
                    .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                        env.put("tax excluded price", env.getInput().get("tax_excluded"));
                    })
                    .And("the VAT rate is <vat_rate>", env -> {
                        env.put("vat rate", env.getInput().get("vat_rate"));
                    })
                    .When("I buy the computer", env -> {
                        double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                        env.put("tax included price", price);
                    })
                    .Then("I need to pay <total_price>", env -> {
                        assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                    })
                    .Execute();
        }
    }
    ```

  - To create a scenario outline with a story, invoke `ScenarioOutline.New(story)`, `ScenarioOutline.New(outlineName, story)` or `ScenarioOutline.New(outlineName, description, story)`.

    ```java
    @EzFeature
    public class ScenarioOutlineExample {
        static Feature feature;
        static Story scenarioOutlineWithTable;

        @BeforeAll
        public static void beforeAll() {
            feature = Feature.New("scenario outline example");
            scenarioOutlineWithTable = feature.newStory("scenario outline with table");
        }

        @EzScenarioOutline
        public void scenario_outline_example_with_table_input() {
            String examples = """
                    | tax_excluded | vat_rate | total_price |
                    | 20000        | 0.05     | 21000       |
                    | 10000        | 0.01     | 10100       |
                    | 35000        | 0.10     | 38500       |
                    """;

            ScenarioOutline.New(scenarioOutlineWithTable)
                    .WithExamples(examples)
                    .Given("the tax excluded price of a computer is <tax_excluded>", env -> {
                        env.put("tax excluded price", env.getInput().get("tax_excluded"));
                    })
                    .And("the VAT rate is <vat_rate>", env -> {
                        env.put("vat rate", env.getInput().get("vat_rate"));
                    })
                    .When("I buy the computer", env -> {
                        double price = env.geti("tax excluded price") * (1 + Double.parseDouble(env.gets("vat rate")));
                        env.put("tax included price", price);
                    })
                    .Then("I need to pay <total_price>", env -> {
                        assertEquals(Double.parseDouble(env.getInput().get("total_price")), env.get("tax included price", Double.class));
                    })
                    .Execute();
        }
    }
    ```

  - Use `@EzScenarioOutline` instead of `@Test`.
  - To use examples as scenario outline input, can invoke three methods with different parameter. To learn more, please refer to the [sample code](../ezspec-sample/src/test/java/tw/teddysoft/ezspec/example/defaultstory/ScenarioOutlineExample.java).

    - `ScenarioOutline::withExamples(example, example...)`

      ```java
      feature.newScenarioOutline()
              .WithExamples(Junit5Examples.get(tax_calculation_example.class), Junit5Examples.get(zero_dollar_invoice_example.class));
      ```

    - `ScenarioOutline::withExamples(listOfExamples)`

      ```java
      List<Example> examples = new ArrayList<>();
      examples.add(Junit5Examples.get(tax_calculation_example.class));
      examples.add(Junit5Examples.get(zero_dollar_invoice_example.class));

      feature.newScenarioOutline()
              .WithExamples(examples);
      ```
  
    - `ScenarioOutline::withExamples(table)`

      ```java
      String examples = """
              | tax_excluded | vat_rate | total_price |
              | 20000        | 0.05     | 21000       |
              | 10000        | 0.01     | 10100       |
              | 35000        | 0.10     | 38500       |
              """;

      feature.newScenarioOutline()
              .WithExamples(examples);
      ```

##### Example

An `Example` class which is a set of test data represents the Examples keyword of Gherkin. A Scenario Outline has one or more Examples. The Scenario Outline is executed once for each row in the Examples section, excluding the initial header row.

To use `Example`, invoke `Example(String tableContent)` and `Example(String name, String description, String tableContent)`.

  Example:

  ```java
  String rawData = """
                | tax_excluded | vat_rate | total_price |
                | 20000        | 0.05     | 21000       |
                | 10000        | 0.01     | 10100       |
                | 35000        | 0.10     | 38500       |
                """;
  Example example = new Example("tax calculation example", "Calculate tax.", rowData);
  feature.newScenarioOutline()
          .withExamples(example)
          .Given(...)
          .When(...)
          .Then(...)
          .Execute();
  ```

##### Junit5Examples

The `Junit5Examples` abstract class is an `Examples` that implements JUnit 5 `ArgumentsProvider` interface to provide test data sets for `ScenarioOutline` in ezSpec as well as for `ParameterizedTest` in JUnit 5.

A concrete Junit5Examples implements `Junit5Example::getExamplesRawData()` to provide the raw data of examples.

Example:

```java
public class tax_calculation_example extends Junit5Examples {
    String rawData = """
            | tax_excluded | vat_rate | total_price |
            | 20000        | 0.05     | 21000       |
            | 10000        | 0.01     | 10100       |
            | 35000        | 0.10     | 38500       |
            """;

    @Override
    public String getDescription() {
        return "Calculate tax.";
    }

    @Override
    protected String getExamplesRawData() {
        return rawData;
    }
}
```

```java
feature.newScenarioOutline()
        .withExamples(Junit5Examples.get(tax_calculation_example.class))
        .Given(...)
        .When(...)
        .Then(...)
        .Execute();
```

Alternatively, a concrete Junit5Examples can implement `Junit5Examples::getTable()` and `Junit5Examples::provideArguments()` to support complex examples data such as a table in a column of each row.

Example:

```java
private static class change_root_stage_order_examples extends Junit5Examples {

    public static final List<String> HEADER = new ArrayList<>(Arrays.asList("example_code", "lane_name", "new_parent_name", "new_position", "event_count", "given_workflow", "expected_workflow"));
    public static final String WORKFLOW = "Workflow";

    @Override
    public String getDescription() {
        return "The order of root stages can be changed.";
    }

    @Override
    protected String getExamplesRawData() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Table getTable() {
        return constructTable(HEADER, provideArguments(null));
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        String givenWorkflow = """
            | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
            | To Do         | root1     |    -1         |   Vertical   |   3        |
            | Doing         | root2     |    -1         |   Vertical   |   5        |
            | Done          | root3     |    -1         |   Vertical   |   -1       |
            """;

        String expectedRootStageToMiddle = """
            | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
            | Doing         | root2     |    -1         |   Vertical   |   5        |
            | To Do         | root1     |    -1         |   Vertical   |   3        |
            | Done          | root3     |    -1         |   Vertical   |   -1       |
            """;

        String expectedRootStageToLast
                = """
            | lane_name     | lane_id   |  parent_id    | lane_layout  | wip_limit  |
            | Doing         | root2     |    -1         |   Vertical   |   5        |
            | Done          | root3     |    -1         |   Vertical   |   -1       |
            | To Do         | root1     |    -1         |   Vertical   |   3        |
            """;

        return Stream.of(
                Arguments.of("ML-D01", "To Do", WORKFLOW, "1", 1, givenWorkflow, expectedRootStageToMiddle),
                Arguments.of("ML-D02", "To Do", WORKFLOW, "2", 1, givenWorkflow, expectedRootStageToLast)
        );
    }
}
```

#### Steps

The `Step` abstract class represents an action taken by a `Scenario`. A Step has a description, a lambda for step definition, and a result of executing the lambda.

The description is a sentence to describe a step and contains arguments for step definitions to write executable specification. The lambda defines the executable code of a step definition, verifying if the production code is compliant with the specification. All the lambdas in a Scenario and the Scenario as a whole form executable specification. The result has four values: Success, Failure, Skip, and Pending. It indicates the executed status of a step.

A Scenario contains a sequence of steps. When the Scenario is executed, each of its steps is executed sequentially. If a step fails, the Scenario is terminated and as a result the remaining steps are not executed. This behavior can be altered by setting the `ContinuousAfterFailure` attribute of Step to true. By so doing, the Scenario continuous its execution even a failed step is encountered.

The Steps have five concrete classes Given, When, Then, And, and But to represent the corresponding Gherkin gherkinKeywords. EzSpec also supports ThenSuccess and ThenFailure to express the two common execution results of the When.

The lambda accepts a parameter `env`, which is a [ScenarioEnvironment](#scenarioenvironment) for getting arguments from description, sharing data among steps, and getting example data.

Step gherkinKeywords provide fluent interface for users to concatenate each step and invoke `Scenario::Execute()` after the last step to execute the test.
Please refer [Usage Example](#usage-example)

```java
feature.newScenario()
    .Given("setup condition", env -> {
    })
    .When("action", env -> {
    })
    .Then("result", env -> {
    })
    .Execute();
```

ezSpec supports concurrent execution of steps via a mechanism called Concurrent Groups. Steps including Given, When, and Then define a concurrent group and act as the first step in the group. Steps such as And and But inside a concurrent group are executed concurrently with the first step. Each concurrent group is executed sequentially. To enable concurrent execution, invoke `Scenario::ExecuteConcurrently()` instead of `Scenario::Execute()`.

Any step failure in a concurrent group terminates the subsequent concurrent groups execution. To continue executing the subsequent concurrent groups while failure, set the `ContinuousAfterFailure` attribute for the step.

```java
feature.newScenario()
    .Given("concurrent group A", env -> {
    })
    .When("concurrent group B", env -> {
    })
    .Then("concurrent group C", env -> {
    })
    .And("concurrent group C", env -> {
    })
    .And("concurrent group C", env -> {
    })
    .Then("concurrent group D", env -> {
    })
    .ExecuteConcurrently();
```

##### ScenarioEnvironment

The ScenarioEnvironment is the parameter passed to the lambda of the step and is used for three purposes: getting arguments from description, sharing data among steps, and getting example data.

- **Getting arguments from step description**: There are two argument types: (1) single value arguments and (2) table arguments.
  - **Single value arguments**: In step description, a word with a prefix "\$" is an anonymous argument that can be accessed with `ScenarioEnvironment::getArgs()` or `ScenarioEnvironment::getArg(int index)` in its step definition. In addition, a word matches the pattern "\${key=value}" or "\${key:value}" is a named argument and can be accessed with the key by invoking `ScenarioEnvironment::getArg(String key)` in its step definition.

    Example:

    ```java
    feature.newScenario()
        .Given("the tax excluded price of a computer is $20,000", env -> {
            assertTrue(env.hasArgument());
            assertEquals(1, env.getArgs().size());
            assertEquals("20,000", env.getArgs().get(0).value());
            assertEquals("20,000", env.getArg(0));
        })
        .And("the VAT rate is ${VAT=5%}", env -> {
            assertEquals(1, env.getArgs().size());
            assertEquals("5%", env.getArg("VAT"));
            assertEquals("VAT", env.getArgs().get(0).key());
        })
        .When("I buy the computer", env ->{})
        .Then("I need to pay ${total_price:21,000}", env ->{
            assertEquals("21,000", env.getArg("total_price"));
        })
        .Execute();
    ```

    ScenarioEnvironment has some help methods to get arguments by key with specific types:

    - to get a string object, invoke `getArg("variable_key")`
    - to get a double object, invoke `getArgd("variable_key")`
    - to get a integer object, invoke `getArgi("variable_key")`

    <br/>

    To get an argument from previous steps, use `getHistoricalArg(index)` and `getHistoricalArg("variable_key")`.
  
  - **Table arguments**: Users define a table in the step description to provide arguments for the step definition. The arguments are retrieved by invoking `ScenarioEnvironment::row(int index)` to get the `Row` of the table, and invoking `Row::get(String header)` to get the column.
  
    Example:

    ```java
    feature.newScenario()
        .Given("""
              |    owner   |   points      |   statusPoints  |
              |    Jill    |   100,000     |   800           |
              |    Jow     |   50,000      |   50            |""", env -> {
            assertEquals("Jill", env.row(0).get("owner"));
            assertEquals("100,000", env.row(0).get("points"));
            assertEquals("800", env.row(0).get("statusPoints"));
        })
    ```

- **Sharing data among steps**: Since step definitions are implemented using Java lambdas, local variables inside the lambda cannot be accessed outside of it. In BDD, a situation may arise where one step definition needs to utilize variables generated in its previous steps. To share data among steps, user can put a value to ScenarioEnvironment with `ScenarioEnvironment::put(String key, Object value)` and access it in later steps with `ScenarioEnvironment::get(String key, Class<T> cls)`.

  ScenarioEnvironment provides following methods to share variables among steps:
  - to define a variable, invoke `env.put("variable_key", variable)`
  - to get a specific type object, invoke `env.get("variable_key", variable_class_name.class)`
  - to get a string object, invoke `env.gets("variable_key")`
  - to get a integer object, invoke `env.geti("variable_key")`
  - to get a table object, invoke `env.gett("variable_key")`

<br/>

- **Getting example data**: In scenario outline, the test data are provided from the Examples. To get the example data in a step, user can invoke `ScenarioEnvironment::getInput().get(String key)`.

#### Dynamic Tests

The Dynamic test is a JUnit 5 feature that generates a test method at runtime. A dynamic test is a container that composes a number of tests in a hierarchical manner.

EzSpec uses the dynamic tests to construct EzDynamicScenario and EzDynamicScenarioOutline that contain a test for each step definition. The purpose of EzDynamicScenario and EzDynamicScenarioOutline is to display detailed test execution results. That is, for each Given, When, Then, And, and But, its description and result are shown in Junit 5 report in an IDE.

EzDynamicScenario report example:
![EzDynamicScenario report example](img/dynamicScenarioReportExample.png)

To use this feature, users need to:

- change `@EzScenario` and `@EzScenarioOutline` to `@EzDynamicScenario` and `@EzDynamicScenarioOutline`, respectively,
- replace the test methods' return type from void to DynamicNode,
- replace `Scenario::Execute()` with `Scenario::DynamicExecute()`, and
- return the result of `Scenario::DynamicExecute()`.

```java
@EzDynamicScenario
DynamicNode EzDynamicScenario_demo() {
    return feature.newScenario()
            .Given(...)
            .When(...)
            .Then(...)
            .DynamicExecute();
}
```

### Living Documentation Support

ezSpec is designed for writing executable specifications. The execution results of ezSpec can be used as living documentation. To support living documentation, ezSpec implements Visitor design pattern so that developers can write their own reports.

A `SpecificationElement` represents a keyword of Gherkin (i.e., Feature, Scenario, Scenario Outline, Given, When, Then, And, and But) and ezSpec (i.e., Story) that can be shown on living documentations. It contains two method: `getName()` and
`accept(SpecificationElementVisitor)`. The former returns name of the keyword, and the latter accepts a `SpecificationElementVisitor` object that is responsible for generating living documentation. An instance of `SpecificationElementVisitor` traverses interested `SpecificationElement` objects and generates a report for living documentation.

ezSpec provides an implementation of `SpecificationElementVisitor`, `PlainTextReport`, in ezSpec-report module that acts as a default report generator when a test class is annotated with `@EzFeatureReport`. See [ezSpec-report](../ezspec-report) for more information.

### Usage Example

Scenario:

```Java
@EzFeature
public class RegisterUserUseCaseTest {
    private static final String FEATURE_NAME = "Register user";
    private static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New(FEATURE_NAME);
        feature.newDefaultStory();
    }

    @EzScenario
    public void register_a_user() {
        feature.newScenario()
                .Given("I am a new user to ezKanban", env -> {
                })
                .When("I register", env -> {
                    RegisterUseCase registerUseCase = new RegisterUseCase(userRepository);
                    RegisterInput input = new RegisterInput();
                    input.setUsername("teddy");
                    input.setPassword("password");
                    input.setEmail("test@gmail.com");
                    input.setNickname("Teddy");
                    var output = registerUseCase.execute(input);
                    env.put("output", output);
                    env.put("input", input);
                })
                .ThenSuccess(env -> {
                    var output = env.get("output", CqrsOutput.class);
                    assertEquals(ExitCode.SUCCESS, output.getExitCode());
                    assertNotNull(output.getId());
                })
                .And("my account is created correctly", env -> {
                    var input = env.get("input", RegisterInput.class);
                    var output = env.get("output", CqrsOutput.class);
                    User user = userRepository.findById(output.getId()).get();
                    assertEquals(input.getUsername(), user.getUsername());
                    Assertions.assertTrue(Encrypt.checkPassword(input.getPassword(), user.getPassword()));
                    assertEquals(input.getEmail(), user.getEmail());
                    assertEquals(input.getNickname(), user.getNickname());
                }).Execute();
    }
}
```

ScenarioOutline:

```Java
@EzFeature
public class ScenarioOutlineSpec {
    private static Feature feature;

    @BeforeAll
    public static void beforeAll() {
        feature = Feature.New("ScenarioOutline is a Scenario with Examples");
        feature.newDefaultStory();
    }

    @EzScenarioOutline
    public void read_data_from_examples_using_correct_table_column_name_and_index() {
        final String examples = """
                 |  tax_included    | VAT   | tax_excluded | notes                 |
                 |  99              | 5     | 94           | rounded               |
                 |  0               | 0     | 0            | zero dollar invoice   |
                 |  1               | 0     | 1            | boundary condition    |
                 |  10              | 0     | 10           | boundary condition    |
                 |  11              | 1     | 10           | boundary condition    |
                """;

        final String expectedResult[][] = {
                {"tax_included", "VAT", "tax_excluded", "notes"},
                {"99", "5", "94", "rounded"},
                {"0", "0", "0", "zero dollar invoice"},
                {"1", "0", "1", "boundary condition"},
                {"10", "0", "10", "boundary condition"},
                {"11", "1", "10", "boundary condition"}
        };

        feature.NewScenarioOutline()
                .WithExamples(examples)
                .Given("a scenario", env -> {
                })
                .And("examples with data set of columns <tax_included>, <VAT>, <tax_excluded>, <notes>", env -> {
                    assertEquals(expectedResult[0][0], env.getInput().header().get(0));
                    assertEquals(expectedResult[0][1], env.getInput().header().get(1));
                    assertEquals(expectedResult[0][2], env.getInput().header().get(2));
                    assertEquals(expectedResult[0][3], env.getInput().header().get(3));
                })
                .When("I run the scenario", env -> {
                })
                .Then("I can read data from the data set with column names", env -> {
                    assertEquals(expectedResult[env.getExecutionCount()][0], env.getInput().get("tax_included"));
                    assertEquals(expectedResult[env.getExecutionCount()][1], env.getInput().get("VAT"));
                    assertEquals(expectedResult[env.getExecutionCount()][2], env.getInput().get("tax_excluded"));
                    assertEquals(expectedResult[env.getExecutionCount()][3], env.getInput().get("notes"));
                })
                .And("with column indexes", env -> {
                    assertEquals(expectedResult[env.getExecutionCount()][0], env.getInput().get(0));
                    assertEquals(expectedResult[env.getExecutionCount()][1], env.getInput().get(1));
                    assertEquals(expectedResult[env.getExecutionCount()][2], env.getInput().get(2));
                    assertEquals(expectedResult[env.getExecutionCount()][3], env.getInput().get(3));
                })
                .Execute();
    }
}
```
