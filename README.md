# ezSpec

ezSpec is a developer-facing Behavior-Driven Development (BDD) tool implemented in Java. It contains two maven projects:

- [ezspec-core](./ezspec-core)
- [ezspec-report](./ezspec-report)
